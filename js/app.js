var app = {
	changeUrl: function(t){
		if(typeof t == 'undefined') {
			var prefs = new Preferences();
			$('#apiUrl').val(prefs.apiUrl).textinput().textinput('refresh');
			//config.apiUrl = prefs.apiUrl;
			user.prefs = prefs;
			api.url = config.apiUrl;
			var db = new simpleDB('config');
			db.put('config', config);
		} else {
			ui.delay(function(){
				var prefs = new Preferences({
					apiUrl:$(t).val()
				});
				config.apiUrl = prefs.apiUrl;
				var db = new simpleDB('config');
				db.put('config', config);
				user.prefs = prefs;
			}, 350);
		}
	},
	
	clear: function(){
		// localStorage.clear();
		// user.logOut();
		var db = new simpleDBPersistent('server');
		db.delete('', function(){
			db = new simpleDBPersistent('serverOperations');
			db.delete('', function(){
				alert("Success!");
			}, function(err){
				alert("failed to delete directory");
			});
		}, function(err){
			alert("Failed to delete directory");
		});
		
	},
	
	enterUrl: function(){
		var url = prompt('Your URL');
		if(url) {
			var url = String(url).replace(/ /g,'')
			config.apiUrl = url;
			api.url = config.apiUrl;
			var db = new simpleDB('config');
			db.put('config', config);
			$('#apiUrl').val(url).textinput().textinput('refresh');
		}
	},
	
	checkSession: function(callback){
		// TODO: replace db session check by putting session in body tag
		//console.log("Checking session..");
		var session = '';
		// if ($('body').attr('data-session')){
		// 	session = JSON.parse($('body').attr('data-session'));
		// }
		session = user.getUserSession();
		
		//var db = new simpleDB('user');
		//var session = db.get('session');
		if(session) {
			//console.log("session object found, checking remote server..");
			api.call('action=checkSession&token='+session.user.token, function(response){
				if(response.output.logged_in === false) {
					//console.log("expired session");
					alert('Your session has expired, please log in.');
					//db.del('session');
					//$('body').attr('data-session', '');
					user.logOut();
				} else {
					if (typeof callback === 'function'){
						callback();
					}
				}
			}, function(){
				// session has expired
				//console.log("expired session");
				alert('Your session has expired, please log in.');
				//$('body').attr('data-session', '');
				user.logOut();
			});
		} 
	},
	
	clearFinder: function(){
		user.device.scanned = {};
		if (typeof user.device.raw != 'undefined') user.device.raw = {};
		$('#tagDisplay').html('');
		$.mobile.loading('hide');
	},
	
	init: function(){
		app.changeUrl();
		app.checkSession();
		interFace.init();
		interFace.initLoginForm();
		/*if ((typeof app.checkSessionInterval != 'undefined') && app.checkSessionInterval)
			clearInterval(app.checkSessionInterval);*/
		app.checkSessionInterval = setInterval(function(){
			app.checkSession();
		}, 300000);		
		api.url = config.apiUrl;
		if(user.loggedIn()) {
			interFace.projects();
			interFace.initGlobalUpdateForm();
			if (api.url.indexOf('frb-richmond') !== -1 || api.url.indexOf('getsimplicity.dev') !== -1){
				frb_richmond.init();
			}
		} else {
			$.mobile.changePage("#logIn");
		}
	},
	
	proData: function(){
		if (api.checkConnection()){
			$.mobile.loading('show');
			$("body").one('proDataFinished',function(resp, error){
				if (error){
					alert(error);
				}
				else {
					alert("Data downloaded to device!");
				}
				$.mobile.loading('hide');
			});
			var syncDataFromServer = function(){
				//console.log("calling pro data request");
				api.call('action=proData&token='+user.session.user.token, function(response){
					var errorResponse = '';
					var dataProcessCount = 0;
					//console.log(response);
					var responseLength = Object.keys(response.output).length;
					var db = new simpleDBPersistent('server');
					$.each(response.output, function(key, value){
						//console.log("putting: "+key);
						db.put(key, value, function(msg){
							++dataProcessCount;
							//console.log(msg);
							//console.log("dataprocesscount = "+dataProcessCount);
							//console.log("response length = "+responseLength);
							if (dataProcessCount >= (responseLength-1)){
								// trigger event
								//console.log("finished processing proData request");
								$('body').trigger('proDataFinished', errorResponse);
							}
						}, function(err){
							//console.log(err);
							errorResponse += err + "\n";
							++dataProcessCount;
							//console.log("dataprocesscount = "+dataProcessCount);
							//console.log("repsonse length = "+response.output.length);
							if (dataProcessCount >= response.output.length){
								// trigger event
								console.log("finished processing proData request");
								$('body').trigger('proDataFinished', errorResponse);
							}
						});
					});
					
				});		
			};
			var db = new simpleDBPersistent('serverOperations');
			//console.log("checking server operations table");
			db.get('', function(response){
				var syncFromServer = true;
				if (typeof response !== 'undefined' && response && Object.keys(response).length > 0){
					syncFromServer = confirm("Unsynced changes detected, proceed? (Warning: this will revert all offline changes)");
				}
				if (syncFromServer){
					//console.log("sync from server");
					db.delete('', function(){
						syncDataFromServer();	
					});
				}
			}, function(err){
				console.log("error getting request, calling sync from server");
				syncDataFromServer();
			});
		} else {
			alert("Device is offline, please connect your device to the internet to proceed.");
		}
	},
	removeAllCache: function(){
	    window.resolveLocalFileSystemURL(cordova.file.cacheDirectory, app.gotDirRemove, function(error){});
	},

	gotDirRemove: function(entry){
		//console.log("got the directory to remove cache");
		//console.log(cordova.file.externalCacheDirectory);
		//console.log(entry.name);
		//console.log(entry);
	    var directoryReader = entry.createReader();
	    directoryReader.readEntries(function(entries) {
	        //console.log("reading entries callback");
	        //console.log(entries.length);
	        var i;
	        for (i = 0; i < entries.length; i++) {
	        	//console.log(entries[i].name);
	            entries[i].remove(function(file){
	            },function(error){
	                });
	        }
	    },function(error){console.log("Failed to list directory contents: " + error.code)});
	},
	switchEncodingType: function(t){
		var prefs = new Preferences();
		prefs.encodingType = ($(t).val() == 'hex' ? enums.encodingType.HEX : enums.encodingType.ASCII);
		user.prefs = new Preferences(prefs);
	},
	sync: function(t){
		// from server operations, get edits
		// if there's more than one request, do sync, else send message saying that client and server are already synced
		// we want to delete the edits from the database, then perform the sync. if we are offline they will go back into edit database
		if (api.checkConnection() && user.getUserSession()){
			app.checkSession(function(){
				$.mobile.loading('show');
				var syncFinished = function(errors){
					// check if there were any api errors, if so present them to the user
					if (errors.length > 0){
						var syncErrorText = "Sync Error\n";
						$.each(errors, function(k, v){
							syncErrorText += v+"\n";	
						});
						alert(syncErrorText);
						$.mobile.loading('hide');
					} else {
						// resync the local data from server
						alert("Remote sync successfull, getting data from server...");
						app.proData();
					}
				}
				// see if there are any changes made to the local data and if so, alert the user before syncing to remote
				var db = new simpleDBPersistent('serverOperations');
				db.get('', function(resp){
					if (typeof resp !== 'undefined' && resp){
						var count = Object.keys(resp).length;
						var canSync = confirm("There are "+count+" offline actions found to sync to the remote server, proceed?");
						if (canSync){
							db.delete('', function(){
								var i = 0;
								var errors = [];
								$.each(resp, function(hash, val){
									//console.log(val)
									var request = $.param(val.request);
									//console.log("Calling: "+request);
									api.call(request+'&token='+user.session.user.token, function(response){
										//console.log(response);
										i++;
										if (i >= count){
											syncFinished(errors);
										}
									}, function(err){
										i++;
										//console.log("Error syncing "+request.action+" action");
										errors.push("Error syncing "+request.action+" action");
										if (i >= count){
											syncFinished(errors);
										}
									});
								});
							}, function(err){
								alert("Sync Error");
								$.mobile.loading('hide');
							});
						}
					}	
				}, function(err){
					alert(err);
					$.mobile.loading('hide');
				});	
			});
		} else {
			alert("Device is offline, please connect your device to the internet to proceed.");
		}
	}
};