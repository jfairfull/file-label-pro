Ready(mobileApp.initialize);
Click('.selector button', interFace.selector);
Change('#projects', project.load);
Change('#viewLists', project.loadList);
Click('#find pre', item.edit);
Click('#inventory pre', item.edit);
Click('.clear', interFace.clear);
Click('.createList', project.createReport);
Click('.createReport', project.createReport);
Click('.inventoryReport', inventory.createReport);
Click('.inventoryList', inventory.createReport);
Click('.inventoryNotInListReport', inventory.createReport);
Click('#logOut', user.logOut);
Click('#forgotPassword', user.forgotPassword);
Submit('#account-form', user.editAccount);
Change('#device', user.changeDevice);
Submit('#globalUpdate form, #globalUpdateScanLocation form', project.globalUpdate);
Submit('#inventoryAppendList form', inventory.appendList);
Click('#inventoryUpdate', inventory.globalUpdate);
Click('#clearFinder', app.clearFinder);
Click('#search', project.searchForm);
Keyup('#apiUrl', app.changeUrl);
Click('#enterUrl', app.enterUrl);
Click('#clearData', app.clear);
Click('#proData', app.proData);
Change('#inventoryLocations select', inventory.changeLocation);
Change('#inventoryLists', inventory.selectList);
Click('#clearInventory', inventory.clear);
Click('#runInventory', inventory.run);
Click('.writeTag', project.writeTag);
Click('.attachImage', project.attachImage);
Click('.tree-btn', interFace.getItemTree);
Change('.encodingType', app.switchEncodingType);
Click('#sync', app.sync)
Click('#revoke', function(){
    if (typeof user.device.revokeTag != 'undefined'){
        user.device.revokeTag();
    }
});
Click('#displayFindImages', interFace.displayFindImages);
Click('.updateAll', function(){
    if (typeof user.session.locationIDs !== 'undefined' && Object.keys(user.session.locationIDs).length > 0 && typeof user.session.locationIDs[Object.keys(user.session.locationIDs)[0]] != 'string'){
        if (confirm("Do you want to scan the location's barcode? (Cancel to choose from dropdown)")){
           $.mobile.changePage("#globalUpdateScanLocation");
        } else {
           $.mobile.changePage("#globalUpdate");
        }   
    } else {
        $.mobile.changePage("#globalUpdate");
    }
});
Click('#homeSwitch', function(){
   $.mobile.changePage("#home"); 
});

// $(document).bind("mobileinit", function(){
//   $.mobile.defaultPageTransition="none";
// });

$(document).on('appReady', 'body', function(){
    document.addEventListener('DOMContentLoaded', function() {
        FastClick.attach(document.body);
        $.mobile.defaultPageTransition="none";
    }, false);
    app.init();
    $.fn.dataTable.ext.errMode = 'none';
});
$(document).on('deviceready', function(){
   if (typeof device.model != 'undefined' && device.model.toUpperCase() == 'AT911N'){
      window.cache_cleaner.delete(function(){/*console.log("cache clean success");*/}, function(){console.log("cache clean fail");});
   }
   
   var i = setInterval(function(){
        if(user.device != 'undefined' && user.loggedIn()) {
            clearInterval(i);
            user.changeDevice(); // already called in app.init through interface.projects?
        }
    }, 1000); 
});
$(document).on('locationConfirmed', '#inventory', function(){
    inventory.getLocation(inventory.sort);
});

$(document).on('battery-indicator-update', function(e, batteryPercentage){
    if (batteryPercentage){
        if (typeof user.device.batteryBar != 'undefined' && user.device.batteryBar){
            //console.log("setting the value for the progress bar");
            user.device.batteryBar.setValue(parseInt(batteryPercentage));
        }
    } else {
        if (typeof user.device.batteryBar != 'undefined' && user.device.batteryBar){
            user.device.batteryBar.destroy();
        }
    }
});

$(document).on('pagechange', function(e, data) {
    // set default of read
    if (typeof user.device != 'undefined'){
        user.device.rfidMode = 'read';
    }
    var page = $(data.toPage).attr('id');
    switch(page) {
        case 'inventory' :
            if (typeof user.device != 'undefined'){
                user.device.rfidMode = 'inventory';
            }
            inventory.ui();
            break;
        case 'tag':
            if (typeof user.device != 'undefined'){
                user.device.rfidMode = 'write';
                
                if (typeof user.device.scanType != 'undefined'){
                    if (user.device.scanType == 'Barcode'){
                        $('#atid-scanType').val('RFID').selectmenu().selectmenu('refresh', true);
                        user.device.switchScanType('#atid-scanType');
                    }
                }
            }
            break;
        case 'rangeFinder':
            if (typeof user.device != 'undefined'){
                user.device.rfidMode = 'find';
            }
            break;
        case 'settings':
            if (typeof user.device != 'undefined' && typeof user.device.checkConnectionStatus != 'undefined'){
                user.device.checkConnectionStatus();
            }
            break;
        case 'home':
            if (typeof device != 'undefined' && typeof device.model != 'undefined' && device.model && device.model.toUpperCase() == 'AT911N'){
                window.cache_cleaner.delete(function(){/*console.log("cache clean success");*/}, function(){console.log("cache clean fail");});
            }    
            break;
        case 'view':
            // interFace.getLists();
            break;
        // allow barcode scanning of locations, switch to barcode scan type if available
        case 'globalUpdateScanLocation':
            if (typeof user.device != 'undefined'){
                if (typeof user.device.barcodeInit === 'function'){
                    if (typeof user.device.scanType !== 'undefined' && user.device.scanType == 'RFID'){
                        user.device.barcodeInit();
                        user.device.scanType = 'Barcode';
                    }
                }
            }
            break;
    }
});

$(document).on('pagehide', function(e, data) {
    var page = $(data.prevPage).attr('id');
    switch(page) {
        // when exiting global update dialogue, if scan type is not the default, then revert
        case 'globalUpdateScanLocation':
            if (typeof user.device != 'undefined'){
                if (typeof user.device.scanType !== 'undefined'){
                    if (user.device.scanType != $('#atid-scanType').val()){
                        user.device.switchScanType('#atid-scanType');
                    }
                }
            }
            break;
    }
});