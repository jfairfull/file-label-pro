/* global deparam */
/* global __ */
/* global simpleDB */
/* global DB */
/* global $ */
/* global DBPersistent */
/* global simpleDBPersistent */
function Server(request, callback) {
    var db = new simpleDBPersistent('server');
    var d;
    var that = this;
    db.get('', function(data){
    	//console.log("got all data in server object");
    	//console.log(request);
    	d = data;
    	that.projectList = d.project;
    	that.data = d.data;
	    that.forms = d.form;
	    that.lists = d.list;
	    that.rfid = d.rfid;
	    //console.log("got rfid");
	    that.alias = d.alias;
	    //console.log("got alias");
	    that.barcode = d.barcode;
	    that.requestString = request;
	    that.locationField = d.locationField;
		that.request = deparam(request);
		if (typeof that.request.data !== 'undefined' && Array.isArray(that.request.data)){
	    	//console.log("converting data array into an object");
			that.request.data = that.request.data.reduce(function(acc, cur, i) {
			  acc[i] = cur;
			  return acc;
			}, {});
		}
	    window.$_REQUEST = that.request;
	    //console.log(typeof window.$_REQUEST);
	    //console.log(typeof window.$_REQUEST['project']);
	    that.response = {
	        error:{},
	        output:{}
	    };
	    that.alterView = [];
	    if (typeof that.forms.display !== 'undefined' && typeof that.forms.display.form !== 'undefined' && that.forms.display.form){
	    	//console.log("number of entries in display form: "+Object.keys(that.forms.display.form).length);
	    	$.each(that.forms.display.form, function(key, field){
	    		//console.log("adding following field to alter view array: "+field.name);
	    		that.alterView.push(field.name);
	    	});
	    }
	    else {
	    	that.alterView = false;
	    }
	    //console.log("checking if function exists: "+that.request.action);
	    //console.log(typeof that[that.request.action]);
	    if(typeof that[that.request.action] == 'function') {
	        that[that.request.action]();
	        if(typeof callback == 'function') {
	            callback(that.response);
	        }        
	    }
	    else {
	    	//console.log("cannot find function");
	    }
    }, function(err){
    	console.log(err);
    });
    
    this.checkSession = function(){
    	this.response['output']['logged_in'] = true;
        return true;
    };
    
    this.proCreateReport = function(){
		if(__('project') && __('data') && __('name')) {
			this.save();
		} else {
			this.response['error']['error'] = 'Please provide a project, report data and a name for the report.';
		}    	
    };
    
    this.proSaveList = function(){
		if(__('project') && __('data') && __('name')) {
			this.save();
		} else {
			this.response['error']['error'] = 'Please provide a project, list data and a name for the list.';
		}    	
    };

	this.form = function(type) {
		//console.log("getting form of type "+type);
		//console.log(typeof this.forms[type]);
	    if(typeof this.forms[type] != 'undefined') {
	        return this.forms[type];
	    } else {
	    	//console.log("getting alias of type "+ type);
	        if(typeof this.alias[type] != 'undefined') {
	        	//console.log(this.alias[type]);
	            return this.forms[this.alias[type]];
	        }
	    }
	    return false;
	};
    
    this.getProjectMeta = function() {
		if(__('project')) {
		    this.response['output'] = this.project(this.request.project);
		    this.response['output']['form'] = this.form(this.request.project);
		} else {
		    this.response['error']['project'] = 'Please provide a project ID.';
		}        
    };
    
    this.proEdit = function(){
    	if(__('project')) {
    		if(__('data')) {
    			this.save();
    		} else {
    			this.response['error']['data'] = 'Please provide some data.';
    		}
    	} else {
    		this.response['error']['project'] = 'Please provide a project ID.';
    	}
    };
    
    this.proFindTag = function(){
        if(__('barcode') || __('tag')) {
            var thing = 'barcode';
            var otherThing = 'tag';
            if(__('barcode')) {
                var db = new DBPersistent('barcode', '', d);
                var otherDB = new DBPersistent('rfid', '', d);
                var value = this.request.barcode;
            }
            if(__('tag')) {
                var db = new DBPersistent('rfid', '', d);
                var otherDB = new DBPersistent('barcode', '', d);
                var thing = 'tag';
                var otherThing = 'barcode';
                var value = this.request.tag;
            }
            var fetch = db.fetch(thing, value);
            if(fetch) {
                var index = this.data[fetch.type][fetch.content_id];
                var otherFetch = otherDB.fetch('content_id', fetch.content_id);
                var info = $.extend(fetch, otherFetch);
                this.response['output'] = {
                    'tag':info.tag,
                    'id':info.content_id,
                    'project':fetch.type,
                    'data':cleanViewOutput(index)
                };
            } else {
                this.response['error']['error'] = 'Not found';
            }
        } else {
            this.response.error['error'] = 'Please provide a barcode or tag value.';
        }
    };
    
    this.proGet = function(){
		if(__('project')) {
			//console.log("found project");
		    var project = this.project(this.request.project);
		    //console.log("got project data for"+this.request.project);
		    var d = this.data[this.request.project];
		    var data = {};
		    $.each(d, function(key, value){
		        data[key] = value;
		        $.each(project.fields, function(k, field){
		            if(typeof data[key][field] == 'undefined') {
		                data[key][field] = '';
		            }
		        });
		        
		        if (this.alterView){
		        	var myData = {};
		        	$.each(value, function(k, v){
		        		if ($.inArray(k, that.alterView)){
		        			myData[k] = v;
		        		}	
		        	});
		        	data[key] = myData;
		        }
		    });
		    this.response['output']['fields'] = project.fields;
		    this.response['output']['form'] = this.form(this.request.project);
		    this.response['output']['data'] = data;
		} else {
			//console.log("Please provide a project id");
			this.response['error']['project'] = 'Please provide a project ID.';
		}
    };
    
    this.proItem = function(){
		if(__('project') && __('id')) {
			var item = this.data[this.request.project][this.request.id];
			if(typeof item == 'undefined') {
				item = false;
			} else {
				var db = new DBPersistent('rfid', '', d);
				var info = db.fetch('content_id', this.request.id);
				item.tag = info.tag;
				db = new DBPersistent('barcode', '', d);
				info = db.fetch('content_id', this.request.id);
				item.barcode = info.barcode;
			}
			this.response['output'] = item;
		} else {
			this.response['error']['error'] = 'Please provide a project ID and an item ID.';
		}
    };

	this.project = function(type) {
		//console.log("getting form");
	    var form = this.form(type);
	    //console.log("got form");
	    var fields = [];
	    if(typeof form == 'object') {
	    	//console.log("form is object");
	        $.each(form.form, function(key, field){
	        	//console.log("looping through fields");
	            fields.push(field.name);
	        });
	    }
	    //console.log("returning from project function");
	    return {
	        name:type,
	        fields:fields,
	        numOfDivs:5,
	        searchForm:form.name,
	        entryForm:form.name,
	        batchUpdateForm:form.name,
	        checkInOutForm:form.name,
	        barcodeField:'',
	        counter:0,
	        counterPrefix:'',
	        switchField:'',
	        minimumQuantity:'',
	        quantityAdmin:''
	    };
	};
    
    this.proSearch = function(){
		if(__('project') && __('data')) {
		    var db = new DBPersistent('data', this.request.project, d);
		    var query = db.query(decodeURI($.param(this.request.data)).replace(/\+/g, ' '));
		    if(query) {
		    	var project = this.project(this.request.project);
		    	this.response['output']['fields'] = project.fields;
		    	this.response['output']['meta'] = {
		    		count:query.length,
		    		max:10000,
		    		start:0
		    	};
		    	this.response['output']['data'] = query;
		    } else {
		    	this.response['error']['error'] = 'No results';
		    }
		} else {
			this.response['error']['error'] = 'Please provide a search query.';
		}
    };
    
    function cleanViewOutput(asset) {
    	var alteredAsset = {};
    	//console.log("CALLING CLEAN VIEW OUTPUT");
    	if (that.alterView){
    		//console.log("looping through asset fields");
			$.each(asset, function(fieldName, field){
				// if field is not in alterview array, delete field
				var index;
				if ((index = $.inArray(fieldName, that.alterView)) == -1){
					delete field[fieldName];
				}
				else {
					alteredAsset[fieldName] = field;
				}
			});
		}
		else {
			alteredAsset = asset;
		}
		
		return alteredAsset;
    };
    
    this.proQuery = function(){
    	if(__('key') && __('val')) {
    		// get listing of asset types
    		var projects = Object.keys(this.projectList);
    		var that = this;
    		if (typeof projects !== 'undefined' && projects){
    			$.each(projects, function(index, projectName){
    				// do a key/value query to get a listing of all assets that match that query
					var db = new DBPersistent('data', projectName, d);
    				var query = db.fetchAll(that.request.key, that.request.val);
    				if (typeof query !== 'undefined' && query){
    					// todo: alter each asset to contain field subset
    					var alteredQuery = {};
    					$.each(query, function(key, value){
    						query[key] = cleanViewOutput(value);		
    					});
						that.response.output['data'] = query;
    				}
    			});
    		}
    	} else {
			this.response['error']['error'] = 'Please provide a valid key/value pair.';
		}
    };
    
    this.proGetTree = function(){
    	if(__('id')) {
    		var myId = this.request.id;
    		var type = false;
    		var parent = false;
    		var foundType = false;
    		var that = this;
    		// get type by id
    		$.each(this.data, function(projectName, val){
    			//console.log("checking: "+projectName);
    			if (foundType){return;}
    			$.each(val, function(id, asset){
    				//console.log("Searching for id in data, id: "+id+" requestid: "+that.request.id);
    				if (id == that.request.id){
    					//console.log("found id");
    					type = projectName;
    					parent = asset.parent;
    					foundType = true;
    					//console.log("found parent: "+asset.parent);
    					return;
    				}
    			});	
    		});
    		var hasType = typeof type !== 'undefined' && type;
    		// while parent exists, search for ancester assets
    	
			var c = 0;
			var hasParent = typeof parent !== 'undefined' && parent;
			//console.log("hasParent truth value: "+hasParent);
			this.response['output'] = [];
    		while(hasParent && c <= 50){
    			var tempParent = parent;
    			parent = false;
    			//console.log("has parent");
    			$.each(this.data, function(projectName, val){
	    			if (foundType){return;}
	    			$.each(val, function(id, asset){
	    				if (id == tempParent){
	    					type = projectName;
	    					parent = asset.parent;
	    					foundType = true;
	    					asset.id = id;
	    					asset.type = type;
	    					that.response['output'].push(asset);
	    					return;
	    				}
	    			});	
	    		});
    			
    			hasParent = typeof parent !== 'undefined' && parent;
    			++c;
    		}
    		// reverse output array for tree structure
    		this.response['output'] = this.response['output'].reverse();
    		
    	} else {
			this.response['error']['id'] = 'Please provide an id.';
		}
    };
    
    this.proInventory = function(){
    	this.response['output']['location'] = false;
		this.response['output']['locations'] = false;
		this.response['output']['locationField'] = false;
		this.response['output']['items'] = {};
		var that = this;
		var tagDB = new DBPersistent('rfid', '', d);
		var barcodeDB = new DBPersistent('barcode', '', d);
		if (__('tags')){
			// get location field
			var locationField = this.locationField;
			var locations = {};
			this.response['output']['locationField'] = locationField;
			if ($.isArray(this.request.tags) || typeof this.request.tags === 'object'){
				var tags = [];
				console.log(this.request.tags);
				if ($.isArray(this.request.tags)){
					// alrh450 webview doesn't support array.keys....
					for (var i in this.request.tags){
						if (!this.request.tags.hasOwnProperty(i)){
							continue;
						}
						if (this.request.tags[i]){
							tags.push(this.request.tags[i]);
						}
					}
				} else {
					tags = Object.keys(this.request.tags);
				}
				console.log(tags);
				$.each(tags, function(key, value){
					console.log(key);
					console.log(value);
					var fetch = tagDB.fetch('tag', value);
					console.log(fetch);
					if (!fetch){
						fetch = barcodeDB.fetch('barcode', value);
					}
					if (fetch){
						//console.log("got fetch object");
						//console.log(fetch.type);
						//console.log(fetch.content_id);
						// get project asset from type and content id
						 var item = that.data[fetch.type][fetch.content_id];
						 console.log(item);
						 if (typeof item !== 'undefined' && item){
						 	//console.log("found item in proInventory endpoint");
						 	if (typeof item[locationField] !== 'undefined' && item[locationField]){
						 		locations[item[locationField]] = typeof locations[item[locationField]] !== 'undefined' && locations[item[locationField]] ? ++locations[item[locationField]] :
						 		1;
						 	}
							console.log(locations);
						 	// todo: get field subset for asset/item
						 	item = cleanViewOutput(item);
						 	// get tag and barcode from content id
							var tagObj = tagDB.fetch('content_id', fetch.content_id);
						 	var barcodeObj = barcodeDB.fetch('content_id', fetch.content_id);
						 	item['tag'] = typeof tagObj.tag !== 'undefined' ? tagObj.tag : '';
						 	item['barcode'] = typeof barcodeObj.barcode !== 'undefined' ? barcodeObj.barcode : '';
						 	console.log(item);
						 	that.response['output']['items'][fetch.content_id.toString()] = item;
							console.log(that.response.output.items);
						 }
					}
				});
				var locationKeys = Object.keys(locations);
				var location;
				if(locationKeys.length) {
					//var locationArray = locationKeys.map(function ( key ) { return locations[key]; });
					var max = -1;
					var index = 0;
					$.each(locations, function(key, value){
						if (max == -1){
							max = value;
							index = key;
						}
						else {
							if (value > max){
								max = value;
								index = key;
							}
						}
					});
					//var value = Math.max.apply( null, locationArray );
					// get the location with the max value
					var value = max;
					location = index;
				}
				this.response['output']['location'] = (typeof location !== 'undefined' && location) ? location : false;
				this.response['output']['locations'] = locationKeys;
				
			}
		} else{
			this.response['error']['error'] = 'Please provide tag values.';
		}
    };
    
    this.proTag = function(){
		// get location field if it exists
		var field = 'location';
		if (typeof this.locationField !== 'undefined'){
			field = this.locationField;
		}
		if(__('tag') && __(field)) {
			var db = new DBPersistent('rfid', '', d);
			var info = db.fetch('tag', this.request.tag);
			if (!info){
				info = db.fetch('barcode', this.request.tag);
			}
			if (info){
				// get asset from info content id
				var item = this.data[info.type][info.content_id];
				if (typeof item !== 'undefined'){
					if (__('status')){
						item.status = this.request.status;
					}
					item[field] = this.request[field];
					this.save();
					this.response['output']['data'] = item;
					this.response['output']['info'] = this.request.tag;
				}
				else {
					//console.log("couldn't find asset");
					this.response['error']['error'] = 'Asset not found.';
				}
			}else{
				this.response['error']['error'] = 'Tag not found.';
			}
			
		} else {
			this.response['error']['error'] = 'Please provide tag status and location.';
		}
    };
    
    this.proGetLists = function(){
    	var userid = user.session.user.userId;
    	if (typeof userid !== 'undefined'){
    		var db = new DBPersistent('list', '', d);
    		var lists = db.fetchAll('user', userid);
    		this.response['output'] = lists;
    	}
    	else {
    		console.log("couldn't find user id");
    		//this.response['error']['error'] = "User id not found, Please login to get lists";
    	}
    };
    
    this.proList = function(){
    	if(__('id')) {
    		var projects = Object.keys(this.projectList);
    		var list  = this.lists[this.request.id];
    		var that = this;
    		if (typeof projects !== 'undefined'){
	    		if (typeof list !== 'undefined'){
	    			$.each(projects, function(key, projectName){
	    				$.each(list.data, function(index, assetID){
	    					//console.log("getting project from data with projectName: "+projectName+" and asset ID: "+assetID);
	    					//console.log(typeof that.data);
	    					var asset = that.data[projectName][assetID];
	    					if (typeof asset !== 'undefined' && asset){
	    						// todo: limit fieldset
	    						asset = cleanViewOutput(asset);
	    						// get tag and barcode and attach it to asset
	    						var db = new DBPersistent('rfid', '', d);
	    						var rfidObj = db.fetch('content_id', assetID);
	    						db = new DBPersistent('barcode', '', d);
	    						var barcodeObj = db.fetch('content_id', assetID);
	    						
	    						asset['barcode'] = typeof barcodeObj.barcode !== 'undefined' ? barcodeObj.barcode : '';
	    						asset['rfid'] = typeof rfidObj.tag !== 'undefined' ? rfidObj.tag : '';
	    						that.response['output'][assetID] = asset;
	    					}
	    				});	
	    			});	
	    		}
    		}
    		
    	} else {
			this.response['error']['error'] = 'Please provide a list ID.';
		}
    }
    
    this.proAppendList = function(){
    	if(__('id') && __('data')) {
    		var list  = this.lists[this.request.id];
    		if (typeof list !== 'undefined'){
    			var listData = !($.isArray(list.data)) ? $.map(list.data, function(value, index) {
				    return [value];
				}) : list.data;
				var appendData = !($.isArray(this.request.data)) ? $.map(this.request.data, function(value, index) {
				    return [value];
				}) : this.request.data;
    			list.data = listData.concat(appendData.filter(function (item) {
				    return listData.indexOf(item) < 0;
				}));
				
				this.response['output'] = list;
				this.save();
    		}
    	} else {
			this.response['error']['error'] = 'Please provide a list ID.';
		}
    };
 
    this.proListFromProject = function(){
    	if(__('id')) {
			if (__('project')){
				var list  = this.lists[this.request.id];
				var project = this.project(this.request.project);
				//console.log("got project data");
				var that = this;
				if (typeof list !== 'undefined'){
					//console.log("looping through list data");
					$.each(list.data, function(key, value){
						// value is asset id, get asset object from value
						//console.log("getting asset of id: "+value+" from project: "+that.request.project);
						//console.log(typeof that.data[that.request.project]);
						//console.log(typeof that.data[that.request.project][value]);
						var asset = that.data[that.request.project][value];
						//todo: restrict fields of asset
						if (typeof asset !== 'undefined'){
							asset = cleanViewOutput(asset);
							that.response['output']['data'] = {};
							that.response['output']['data'][value] = asset;
						}
					});
					this.response['output']['fields'] = project.fields;
				}
			} else {
				this.response['error']['error'] = 'Please provide a project.';
			}
		} else {
			this.response['error']['error'] = 'Please provide a list ID.';
		}
    };
    
    this.proAttach = function(){
    	if(__('id') && __('image') && __('project')) {
    		var project = this.project(this.request.project);
    		var hasImageField = false;
    		$.each(project.fields, function(key, value){
    			if ('image' == value){
    				hasImageField = true;
    			}	
    		});
    		if (hasImageField){
    			this.save();
    		}
    		else{
    			this.response['error']['image'] = 'This project does not contain an image field.';
			}
    	} else {
			this.response['error']['error'] = 'Please provide a project, ID an base64 image.';
		}
    };
    
    this.save = function(){
    	//console.log("instantiating server operations db call");
    	var db = new simpleDBPersistent('serverOperations');
    	var ops = {}; 
    	var that = this;
    	//console.log("getting edits");
    	db.get('', function(resp){
			saveRequest(resp);
    	}, function(err){
    		saveRequest(false);
    	});
    	
    	function saveRequest(resp){
    		if (resp !== 'undefined' && resp){
    			//console.log("found existing response");
    			ops = resp;
    		} else {
    			ops = {};
    		}
			//console.log("pushing hash");
			delete that.request.token;
			var hash = CryptoJS.SHA1(JSON.stringify(that.request));
			ops = {
	    		'hash':hash+"",
	    		'request':that.request
	    	};
	    	//console.log("hash: "+hash);
	    	//console.log(ops);
	    	db.put(hash+"", ops, function(msg){
	    		//console.log(msg);
	    	}, function(err){
	    		//console.log(err);
	    		alert(err);
	    	});		
    	};
    	// var ops = (db.get('edits') ? db.get('edits') : {});
    	// delete this.request.token;
    	// var hash = CryptoJS.SHA1(JSON.stringify(this.request));
    	// ops.push({
    	// 	'hash':hash,
    	// 	'request':this.request
    	// });
    	// db.put('edits', ops);    	
    };
    
}