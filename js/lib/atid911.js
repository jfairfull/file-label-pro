var atid911 = {
    isBluetooth:false,
    scanType: '',
    rfidMode: 'read',
    range:{
        low: -85,
        high: -30
    },
    hasInitialized: false,
    isWriting : false,
    powerLevel: 300,
    myactiveTag: '3000AD9009001B6BFD9662000018',
    tagsInRange: {},
    isReading:false,
    
    // TODO: create enums/objects for rfid modes and rfid write fail types. might be useful to create a separate atid state manager for modes/boolean flags
    // TODO: implement continuous burst for inventory screen while find view does single read?
    init: function(isReset){
        console.log('ATID!!!');
        
        // populate widgets with power level and scan type from preferences, TODO: initialize these elsewhere in a different namespace
        if (typeof isReset == 'undefined'){
            user.device.populatePowerLevel();
            user.device.loadScanType();
        }
        
        if(cordovaApp) {
            user.device.tagsInRange = {};
            user.device.switchScanType('#atid-scanType');
            user.device.enableKeyEvents();
        }
      
    },
    switchScanType : function(t){
        var type = $(t).val();
        
        switch(type){
            case('RFID'):
                user.device.rfidInit();
                user.device.scanType = type;
                break;
            case('Barcode'):
                user.device.barcodeInit();
                user.device.scanType = type;
                break;
            
        }
        
        
        
        if (typeof user.prefs == 'undefined')
            user.prefs = new Preferences();
            
        user.prefs.scanType = user.device.scanType;
        
        user.prefs = new Preferences({'scanType':user.prefs.scanType});
    },
    rfidInit : function() {
        user.device.hasInitialized = false;
        
        //console.log("Initializing atid rfid!");
        
       if (user.device.scanType == 'Barcode'){
            atid.barcode.sleep();
            atid.rfid.wakeup(function() {
                user.device.hasInitialized = true;
                user.device.changePowerLevel();
            });
       }
       else{
           //atid.rfid.sleep();
           atid.rfid.wakeup(function(msg) {
                //console.log(msg);
                user.device.reset = false;
            });
       
           
       }
       
        // ### called when rfid tag is read ###
        atid.rfid.onReaderReadTag(user.device.readTagCallback, user.device.printMsg);
        // ### called when tag write/read memory is invoked ###
        atid.rfid.onReaderResult(user.device.writeTagCallback, user.device.printMsg);
        // ### called when rfid reader state has changed
        atid.rfid.onReaderStateChanged(user.device.stateChangeCallback, user.device.printMsg);
        // ### called when rfid reader action has changed
        atid.rfid.onReaderActionChanged(user.device.actionChangeCallback, user.device.printMsg);
    
    },
    stateChangeCallback : function(state) {
      //console.log(state);
      if (state == "Connected"){
          user.device.hasInitialized = true;
          user.device.changePowerLevel();
      }
    },
    actionChangeCallback : function(action){
        //console.log(action);
        if (action == "Stop Operation"){
            user.device.isReading = false;
        }
    },
    readTagCallback : function(data) {
        //atid.general.playSound('beep');
          
          
          if (user.device.rfidMode == 'read' || user.device.rfidMode == 'inventory'){
            user.device.processingTag = true;
            //console.log("Tag : " + data.tag + " RSSI: " + data.rssi);
            if($.mobile.activePage.attr("id") != 'rangeFinder') {
                user.device.go(data.tag);
            }
          }
          
          // If writing, proceed with verification step
          else if (user.device.rfidMode == 'write'){
             //console.log("Tag : " + data.tag + " RSSI: " + data.rssi);
            user.device.tagsInRange[data.tag] = data.tag;
            var tag = user.device.processRawTag(data.tag);
            
            if (tag == user.device.activeTag){
                 $.mobile.loading('hide'); 
                 //console.log("RFID Write succeeded");
                 atid.general.onKeyUp(user.device.KeyUpCallback);
                 atid.general.onKeyDown(user.device.KeyDownCallback);
                 user.device.enableUI();
                 
                 alert("RFID Write Succeeded");
            }
            else{
                 user.device.writeRfidError("Verification fail");
            }
          }
          
          else if (user.device.rfidMode == 'revoke'){
              //console.log("Tag : " + data.tag + " RSSI: " + data.rssi);
              var tag = data.tag.length == 28 ? $.trim(data.tag.substring(4, data.tag.length)) : $.trim(data.tag);
              if (tag == '000000000000000000000000'){
                 $.mobile.loading('hide'); 
                 //console.log("RFID revoke succeeded");
                 user.device.enableKeyEvents();
                 user.device.enableUI();
                 
                 alert("RFID Revoke Succeeded");
            }
            else{
                 user.device.writeRfidError("Possible revoke fail");
            }
          }
          
          else if (user.device.rfidMode == 'find'){
            
            if (typeof user.device.tagsInRange[data.tag] == 'undefined'){
                user.device.tagsInRange[data.tag] = data.tag;
                var t = user.device.processRawTag(data.tag);
                if (user.device.tagToFind == t){
                     //console.log("Tag : " + data.tag + " RSSI: " + data.rssi);
                    atid.general.playSound('beep');
                    $('#range').val(data.rssi).slider("refresh");
                }
            }
              
          }
    },
    writeTagCallback : function(data){
        //console.log('code: ' + data.code + ' action: ' + data.action + 'epc: ' + data.epc + 'data: ' + data.data);
        
        if (user.device.rfidMode == 'write' || user.device.rfidMode == 'revoke'){
        
            if (data.code == "Out of retries"){
                user.device.writeRfidError(data.code);
                
            }
            else{
                user.device.writeOffset += 1;
                setTimeout(function(){
                    user.device.recursiveWrite(user.device.writeOffset, user.device.writeData);
                    
                }, 1000); 
            }
        }
    },
    barcodeInit : function() {
        //console.log("Initializing atid barcode!");
        user.device.hasInitialized = false;
        if (user.device.scanType == "RFID"){
            atid.rfid.sleep();
            atid.barcode.wakeup(function(msg) {/*console.log(msg);*/ user.device.hasInitialized = true;});
        }
        else{
           atid.barcode.wakeup(function(msg) {/*console.log(msg);*/ user.device.hasInitialized = true;}); 
        }
            
        atid.barcode.onDecode(function(scanResults){
                  //console.log(scanResults.type + ' ' + scanResults.barcode);
                  atid.general.playSound('success');
                  //atid.barcode.stopDecode(function(){}, function(){});
                  if($.mobile.activePage.attr("id") != 'rangeFinder') {
                    user.device.go(scanResults.barcode);
                 }
              }, function(msg){/*console.log(msg);*/ atid.general.playSound('fail');});
    },
    //TODO: make the process raw tag and decode processed tag in a separate namespace
    processRawTag: function(data){
        return invengo_device.processRawTag(data);
//         var x = '';
                    
//         // check for header hex chars and remove, TODO: find out what header does, scanned tags seem to start with '3000'?
//         if (data.length == 28){
// 		    x = $.trim(data.substring(4, data.length));
//         }
//         else
//             x = $.trim(data);

		
// 		// get char code for every 8 bit hex value in raw tag
// 		var a = x.match(/.{1,2}/g);
// 		var t = '';
// 		$.each(a, function(k, v){
// 		    //console.log("hex transform: " + v);
// 		    //console.log("corresponding decimal value: " + hex2dec(v));
// 			t += $.trim(String.fromCharCode(hex2dec(v)));
// 		});
		
// 		return t;
    },
    decodeProcessedTag: function(tag){
        var data = '';
        for (var i = 0; i < tag.length; i++){
            data += dec2hex(tag.charCodeAt(i));
        }
        
        //console.log(data);
        return data;
    },
    go: function(data){
		if(typeof data != 'undefined') {
		    //console.log(user.device.scanType);
			if (user.device.scanType == 'RFID'){
			    if (typeof user.device.raw[data] == 'undefined') {
			        // if we are reading in a new tag into the system, shut down key event listener, stop any scan, process tag/find, then reinitialize
			        user.device.raw[data] = data;
            		$.mobile.loading('show');
            		var t = user.device.processRawTag(data);
            		
            	    //console.log('Encoded RFID value: ' + t);
            	    t = t.toLowerCase();
            		
            		if(typeof user.device.scanned[t] == 'undefined') {
            		    t = t.toLowerCase();
            			user.device.scanned[t] = t;
            			atid.general.playSound('beep');
            			
            			if($.mobile.activePage.attr("id") == 'inventory') {
            				inventory.refresh(t);
            			} else {
            				user.device.find(t, 'rfid');
            			}
            		}
            		else
            		    user.device.processingTag = false;
    			}
    			else
    			   user.device.processingTag = false; 
			}
            else if (user.device.scanType == 'Barcode'){
        		$.mobile.loading('show');
        		var x = data;
        		x = x.toLowerCase();
        		if(typeof user.device.scanned[x] == 'undefined') {
        		    x = x.toLowerCase();
        			user.device.scanned[x] = x;
        			//user.device.find(x, 'barcode');
        			if($.mobile.activePage.attr("id") == 'inventory') {
            			inventory.refresh(x, 'barcode');
        			} else {
        				user.device.find(x, 'barcode');
        			}
    				
    			}
            }
			
		}
	},
	find: function(t, type){
		var myType = (type == 'undefined' ? 'rfid' : type);
		var apiCallType = (myType == 'rfid' ? 'tag' : 'barcode');
		if(t.length == 8 || t.length == 6 || t.length == 12 || t.length == 13 || t.length == 24) {
			var found = false;
			api.call('action=proFindTag&'+apiCallType+'='+t+'&token='+user.session.user.token, function(response){
				item.display(response, t, 'rfid');
				//console.log("Found tag in remote server!");
				$.mobile.loading('hide');
				user.device.processingTag = false;
			}, function(){
			    alert("error when reaching findTag endpoint, check network connection");
				$.mobile.loading('hide');
				user.device.processingTag = false;
			});
			$('body').trigger('findTag');
		} else {
			alert('The tag is not valid.');
			//console.log('The tag is not valid.');
			user.device.processingTag = false;
		}
	},
	rangeFinder: function(t){
	    //console.log("Opening range finder");
	    if (user.device.disableKeyEvents()){
        	$('#range').attr('min', user.device.range.low);
        	$('#range').attr('max', user.device.range.high);
        	$.mobile.changePage('#rangeFinder');
        	$.mobile.loading('show');
        	var readCount = 0;
        	var resetReadCount = 10;
        	var id = $(t).attr('data-id');
        	api.call('action=proItem&project='+$('#projects').val()+'&id='+id+'&token='+user.session.user.token, function(response){
        		$.mobile.loading('hide');
        		if(cordovaApp && response.output) {
        		    user.device.tagToFind = response.output.tag;
        		    atid.rfid.isStopped(function(msg){
                          if (msg == 'true'){
                              
                              
                            user.device.isScanning = true;
                            user.device.scanBurst();
                            // atid.rfid.setIdleTime(1000, function(){
                            //     atid.rfid.stop_scan(function(){
                            //          atid.rfid.start_readTagContinuous(user.device.printMsg, user.device.printMsg, false);
                            //     });
                            // }, user.device.printMsg);
                              /*  
                              user.device.finderInterval = setInterval(function(){
                		        atid.rfid.stop_scan(user.device.printMsg, user.device.printMsg);
                		    }, 1000);          
                              */
                          }
                          
                    });
        		    
        		   
        		}
        	});
	    }
	},
	scanBurst : function(){
	    if (!user.device.isScanning)
	        return;
	        
	    atid.rfid.stop_scan(function(){
	        user.device.tagsInRange = {};
	        setTimeout(function(){
	            if (user.device.isScanning){
    	            atid.rfid.start_readTagContinuous(function(){
        	            setTimeout(function(){
        	                if (user.device.isScanning == true)
        	                    user.device.scanBurst();
        	            }, 500);
	                }, user.device.printMsg, false);
	            }
	        }, 2000);
	    }, user.device.printMsg);
	},
	closeRangeFinder: function(){
	    //console.log("closing range finder");
	    user.device.isScanning = false;
		atid.rfid.stop_scan();
		user.device.tagsInRange = {};
		
		user.device.tagToFind = '';
		$.mobile.changePage('#view');
		user.device.enableKeyEvents();
	},
	writeRfid : function(data) {
	    user.device.tagsInRange = {};
	    if (data.length != 24){
	        alert("invalid tag to write, aborting..");
	        user.device.enableKeyEvents();
	        return;
	    }
	    
	    user.device.rfidMode = user.device.rfidMode == 'revoke' ? 'revoke' : 'write';
	    $.mobile.loading('show');
	    user.device.disableUI();
	    
	    // The most elegant solution!
	    // disable key events, will return true when complete
	    if (user.device.disableKeyEvents()) {
	        //console.log("disabling key events");
	        
	        // stop any ongoing scans
            atid.rfid.stop_scan(function(){
                // set a timeout in case write never finishes
                user.device.writeTimeout = setTimeout(function(){
                    if (user.device.isWriting)
                        user.device.writeRfidError("RFID Write Timed Out");
                }, 20000);
                user.device.isWriting = true;
                //console.log("writing " + data + " to RFID chip!");
                user.device.writeOffset = 2;
                user.device.writeData = data;
                user.device.recursiveWrite(user.device.writeOffset, data);
    
            },  user.device.writeRfidError);
	    }
	    
	},
	// have to write one word (4 hex chars) at a time due to exception
	recursiveWrite : function(offset, data){
	    // base case, end of write
	    if (offset > 7){
	         user.device.writeOffset = 2;
	         user.device.isWriting = false;
	         
	         //console.log("Verifying write...")
	         // verify that the tag written is equal to data
	         atid.rfid.isStopped(function(msg){
	             if (msg == 'true'){
	                atid.rfid.start_readTagSingle(user.device.printMsg, function(msg){user.device.writeRfidError(msg)}, false);        
	             }
	             else{
	                 //console.log("rfid not stopped, retrying...");
	                 setTimeout(function(){
	                     atid.rfid.isStopped(function(msg){
            	             if (msg == 'true'){
            	                atid.rfid.start_readTagSingle(user.device.printMsg, function(msg){user.device.writeRfidError(msg)}, false);       
            	             }
            	             else
            	                user.device.writeRfidError("RFID Halting error");
	                     });
        	             
	                 }, 500);
	             }
	         });

	         return;
	    }
	    else{
	        var pos = (offset-2)*4;
	        //console.log("writing " + data.substring(pos, pos+4) + " to RFID chip!, position: " + pos);
	        
	        // TODO: if we call this function, but get no tags from the write tag callback, we are out of range and need to throw appropriate error (Set a write timeout perhaps??)
            atid.rfid.start_writeTagMemory({
              'bankType' : 'EPC',
              'offset' : offset,
              'password' :  '',
              'data' : data.substring(pos, pos+4 )
            }, user.device.printMsg, user.device.printMsg);
	    }
	},
	writeRfidError : function(msg){
	  clearTimeout(user.device.writeTimeout);
	  user.device.isWriting = false;
	  user.device.rfidMode = 'write';
	  //console.log("Write error");
	  //console.log(msg);
	  alert(msg);
	  atid.rfid.stop_scan(function(){
	      user.device.init();
          atid.general.onKeyUp(user.device.KeyUpCallback);
          atid.general.onKeyDown(user.device.KeyDownCallback);
	         
	      $.mobile.loading('hide');
	      user.device.enableUI();
	  });
	    
	},
	revokeTag: function(){
	    //console.log("Revoking tag!");
        user.device.rfidMode = 'revoke';
        user.device.writeRfid('000000000000000000000000');  
	},
	disableKeyEvents : function(){
	    return (atid.general.onKeyDown() == atid.general.onKeyUp());
	},
	enableKeyEvents : function() {
	    atid.general.onKeyUp(user.device.KeyUpCallback);
        atid.general.onKeyDown(user.device.KeyDownCallback);
	},
    KeyUpCallback : function(key) {
        //$.each(user.device.scanned, function(k, v){console.log("Scanned Values: " + v);});
        //atid.rfid.getPower(function(msg){console.log(msg)});
        switch (user.device.scanType){
            case ('RFID'):
                if (key.keyCode == atid.general.scanner_handle_keycode && key.repeatCount <= 0){
                  
                  
                  //atid.rfid.isStopped(function(msg){
                      //console.log("processing isStopped result");
                      //if (msg == 'false'){
                          //console.log("calling stop scan");
                          atid.rfid.stop_scan(function(msg){
                            //console.log(msg);
                            //console.log("stop scan invoked, called success callback");
                          }, function(msg){
                              //console.log(msg);
                              //console.log("stop scan invoked, called error callback");
                          });
                          //$.mobile.loading('hide');
                      //}
                  //});
                }
                break;
            
            case('Barcode'):
                 if (key.keyCode == atid.general.scanner_handle_keycode && key.repeatCount <= 0){
                  atid.barcode.isDecoding(function(msg){
                        if (msg == 'true')
                        {
                            atid.barcode.stopDecode();
                            //$.mobile.loading('hide');
                        }
                    }, user.device.printMsg);
                }
                break;
            
        }
    },
    
    KeyDownCallback : function(key) {
        
        // TODO: additional control logic for more scan types under rfid, such as writing tags.
        // TODO: switch rfid mode on tag write screen, if mode is write, process active tag into hex and send into writerfid function
        switch (user.device.scanType){
            case ('RFID'):

                if (key.keyCode == atid.general.scanner_handle_keycode && key.repeatCount <= 0){
                    if (user.device.rfidMode == 'read'){
                        if (!user.device.isReading && !user.device.processingTag){
                              atid.rfid.isStopped(function(msg){
                                  if (msg == 'true'){
                                      user.device.isReading = true; 
                                      //atid.rfid.start_readTagSingle(function(msg){console.log(msg);}, user.device.printMsg /*user.device.rfidReadError*/, false);
                                      atid.rfid.start_readTagContinuous(function(msg){/*console.log(msg)*/}, user.device.printMsg/*user.device.rfidReadError*/, false);
                                  }
                                  
                                      
                                      /*atid.rfid.start_readTagMemory({
                                          'bankType' : 'EPC',
                                          'offset' : 2,
                                          'length' : 2,
                                          'password' :  ''
                                      }, user.device.printMsg, user.device.printMsg);*/
                              }, user.device.printMsg);
                        }
                    }
                    else if (user.device.rfidMode == 'write' && key.repeatCount <= 0){
                        if (typeof user.device.activeTag == 'undefined' || user.device.activeTag == ''){
                            //console.log("active tag not found, aborting write...");
                            user.device.writeRfidError("active tag not found, aborting write...");
                            return;
                        }
                            
                        var data = '';
                        //console.log(user.prefs.encodingType);
                        //console.log(enums.encodingType.HEX);
                        if (user.prefs.encodingType == enums.encodingType.HEX){
                            for (var i = 0; i < user.device.activeTag.length; i++){
                                data += dec2hex(user.device.activeTag.charCodeAt(i));
                            }
                        } else {
                            data = user.device.activeTag;
                        }
                        
                        //console.log(data);
                        
                        if (user.device.disableKeyEvents())
                        {
                            user.device.writeRfid(data);
                        }
                    }
                    else if (user.device.rfidMode == 'inventory'){
                        atid.rfid.isStopped(function(msg){
                              if (msg == 'true'){
                                  user.device.isReading = true; 
                                  atid.rfid.start_readTagContinuous(function(msg){/*console.log(msg);*/}, user.device.printMsg/*user.device.rfidReadError*/, false);
                              }
                          }, user.device.printMsg);
                    }
                }
                break;
            
            case('Barcode'):
                if (key.keyCode == atid.general.scanner_handle_keycode && key.repeatCount <= 0){
                    atid.barcode.isDecoding(function(msg){
                        if (msg == 'false')
                        {
                            atid.barcode.startDecode();
                        }
                    }, function(){});
                }
                break;
        }
        
    },
    rfidReadError: function(error){
      //console.log(error);
      if (error.indexOf('Busy') !== -1){
          // disable key callbacks, stop any scanning, and reinitialize
          user.device.rfidReset();
      }
      
    },
    rfidReset : function(){
        if (!user.device.reset){
            //console.log("reseting rfid device");
            user.device.reset = true;
            $.mobile.loading('show');
            user.device.disableUI();
            atid.rfid.forceSleep(function(){
              //console.log("sleeping device...");
              if (user.device.disableKeyEvents){
                  user.device.switchScanType('#atid-scanType');
                  user.device.enableKeyEvents();
                  $.mobile.loading('hide');
                  user.device.enableUI();
              }
            });
        }
    },
    loadScanType: function(){
        if (typeof user.prefs == 'undefined'){
        	user.prefs = new Preferences();
        }
        
        if (typeof user.prefs.scanType == 'undefined') {
            user.prefs = new Preferences({'scanType':'RFID'});
        }
            
        $('#atid-scanType').val(user.prefs.scanType);
	
      
    },
    changePowerLevel: function(t){
        

        user.device.disableUI();
        
        var $power_selects = $('.power-level');
		var powerLevel = (typeof t == 'undefined') ? '' : $(t).val();
		
		// update preferences with selected power level
		if (typeof user.prefs == 'undefined')
			user.prefs = new Preferences();
		
		if (powerLevel){
			user.prefs.powerLevel = powerLevel;
			
			// update all other power slider selects
    		$.each($power_selects, function(){
    		    $(this).val(powerLevel).selectmenu().selectmenu('refresh');
    		});
		}
		else if (typeof user.prefs.powerLevel == 'undefined'){
			user.prefs.powerLevel = (typeof user.device.powerLevel) != 'undefined' ? user.device.powerLevel : 300;
		}
		
	    
	    user.device.powerLevel = user.prefs.powerLevel;
		
		user.prefs = new Preferences({'powerLevel':user.prefs.powerLevel});
		
		// if the device is initialized, set the power level through the atid rfid api
		if (user.device.hasInitialized){
		    //console.log("changing power to: " + user.device.powerLevel);
		    atid.rfid.setPower(user.device.powerLevel, user.device.printMsg, user.device.printMsg); 
		}
		else{
		    //console.log("Device couldn't set power, not initialized");
		}
		    
		user.device.enableUI();
		
	},
	populatePowerLevel: function(){
	  // grap select statement by class, dynamically add options 100-300 with increment level 50
	  // once select is populated, make the value = to the loaded preferences option
	  
	  if (typeof user.prefs == 'undefined')
	    user.prefs = new Preferences();
	    
	  if (typeof user.prefs.powerLevel == 'undefined'){
	      user.prefs = new Preferences({'powerLevel':'300'});
	  }
	  var $select = $('.power-level');
	  var options = '';
	  
	  for (var i = 100; i <= 300; i+=50)
	  {
	      options += '<option value="'+i+'">'+i+'</option>';
	  }
	  
	  $.each($select, function(){
	      $(this).html(options);
	      $(this).val( user.prefs.powerLevel ).selectmenu().selectmenu('refresh');
	  });
	  
	},
	switchRfidMode : function(mode){
	  switch(mode){
	      case('read'):
	      case('write'):
	          user.device.rfidMode = mode;
	          break;
	      default:
	        console.log("Invalid rfid mode set");
	    
	  }  
	},
    printMsg : function(msg) {console.log(msg)},
    disableUI : function(){
        $("body").prepend("<div class=\"overlay\"></div>");

        $(".overlay").css({
            "position": "absolute", 
            "width": $(document).width(), 
            "height": $(document).height(),
            "z-index": 99999,
            "opacity":0.8,
        }).fadeTo(0, 0.8);
    },
    enableUI : function(){
      $(".overlay").remove();  
    },
    raw:{},
    scanned:{}
};