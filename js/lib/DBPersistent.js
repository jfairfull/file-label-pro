// passes in the data directly instead of reaching out to simpleDB 
function DBPersistent(type, subType, data) {
    this.type = type;
    this.subType = (typeof subType == 'undefined' ? '' : subType);
    this.server = data[type];

    this.count = function(mixed_var, mode) {
        var key, cnt = 0;
        if (mixed_var === null || typeof mixed_var === 'undefined') {
            return 0;
        } else if (mixed_var.constructor !== Array && mixed_var.constructor !== Object) {
            return 1;
        }
        if (mode === 'COUNT_RECURSIVE') {
            mode = 1;
        }
        if (mode != 1) {
            mode = 0;
        }
        for (key in mixed_var) {
            if (mixed_var.hasOwnProperty(key)) {
                cnt++;
                if (mode == 1 && mixed_var[key] && (mixed_var[key].constructor === Array || mixed_var[key].constructor === Object)) {
                    cnt += this.count(mixed_var[key], 1);
                }
            }
        }
        return cnt;
    };    
    
    this.fetch = function(field, value) {
        var found = false;
        $.each(this.server, function(k, v){
            if(v[field] == value) {
                found = v;
                return; 
            }
        });
        return found;
    };
    
    this.fetchAll = function(field, value) {
        var found = false;
        var out = {};
        var objToSearch = this.subType ? this.server[this.subType] : this.server;
        $.each(objToSearch, function(k, v){
            if(v[field] == value) {
                out[k] = v;
            }
        });
        return Object.keys(out).length > 0 ? out : false;
    };

    this.objectToQueryString = function(obj) {
        var output = '';
        var count = 0;
        for (var key in obj) {
            if (obj.hasOwnProperty(key)) {
                output += key + '=' + obj[key] + '&';
            }
        }
        return output.slice(0, -1);
    }    
    
    this.parseStr = function(q) {
        var arr = {};
        var query = String(q).split('&');
        for (var key in query) {
            if (query.hasOwnProperty(key)) {
                var str = String(query[key]).split('=');
                var va = str[1];
                arr[str[0]] = str[1];
            }
        }
        return arr;
    }    

    this.query = function(q) {
        var arr = this.parseStr(q);
        var get = this.server[this.subType];
        var out = {};
        var empty = true;
        if (get) {
            for (var key in get) {
                if (get.hasOwnProperty(key)) {
                    var objectString = this.objectToQueryString(get[key]);
                    if (this.testQueryArray(arr, objectString)) {
                        var empty = false;
                        out[key] = get[key];
                    }
                }
            }
        }
        return (empty ? false : out);
    };

    this.testQueryArray = function(array, objectString) {
        var testArray = [];
        for (var key in array) {
            if (array.hasOwnProperty(key)) {
                testArray.push(key + '=' + array[key]);
            }
        }
        var count = this.count(testArray);
        var testCount = 0;
        for (var key in testArray) {
            if (testArray.hasOwnProperty(key)) {
                if (objectString.indexOf(testArray[key]) !== -1) {
                    testCount++;
                }
            }
        }
        return (count == testCount ? true : false);
    }    
};