var frb_richmond = {
	siteFloorMap:{},
	site:false,
	floor:false,
	eventsAttached:false,
	init: function(){
		// place queryinventorylist button right under list dropdown
		if (!$('#queryFloorInventoryList').length){
			$('#inventoryPanel').find('label:first').after('<button id="queryFloorInventoryList" class="ui-btn">Query Inventory</button>');
		}
		api.call('action=proGetSiteFloorMap&token='+user.session.user.token, function(response){
			frb_richmond.siteFloorMap = response.output;
		});
		if (!frb_richmond.eventsAttached){
			frb_richmond.events();
		}
	},
	events: function(){
		// on list selection, override Click('#runInventory', inventory.run);
		// call save inventory to set data parameter, sets items to in location
		// clear screen, reset run inventory function
		Click('#queryFloorInventoryList', frb_richmond.queryListModal);
		Change('#siteSelect', function(t){
			var site = $(t).val();
			$("#floorInventoryListModal select#floorSelect").html('<option value="">Please Choose</option>');
			if (typeof frb_richmond.siteFloorMap[site] !== 'undefined'){
				$.each(frb_richmond.siteFloorMap[site], function(key, value){
					$("#floorInventoryListModal select#floorSelect").append('<option value="'+value+'">'+value+'</option>');
				});
			}
		});
		Click('#floorInventoryListModal button', function(t){
			var site = $('#siteSelect').val();
			var floor = $('#floorSelect').val();
			frb_richmond.queryList(site, floor);
		});
		$(document).off('#clearInventory');
		Click('#clearInventory', frb_richmond.reset);
		$(document).on('click', '#saveFloorInventoryList', frb_richmond.saveList);
	},
	reset: function(){
		$('#saveFloorInventoryList').remove();
		$('#runInventory').css('display', 'block');
		inventory.clear();
	},
	queryList: function(site, floor){
		frb_richmond.site = site;
		frb_richmond.floor = floor;
		$('#saveFloorInventoryList').remove();
		$('#runInventory').before('<button id="saveFloorInventoryList" class="ui-btn">Save Inventory</button>');
		$('#runInventory').css('display', 'none');
		$.mobile.changePage('#inventory');
		api.call('action=proFloorInventoryList&site='+site+'&floor='+floor+'&token='+user.session.user.token, function(response){
			frb_richmond.populateList(response.output);
		});
	},
	populateList: function(list){
		// get list, populate in location and not in location views
		inventory.clear();
		$.each(list.in_location, function(key, value){
			//inventory.list[value.tag] = value.tag;
			//inventory.list[value.barcode] = value.barcode;
			value.id = key;
			inventory.display(value, '#il');
			inventory.display(value, '#inventoryDisplay');
			inventory.removeDuplicates(['#inventoryDisplay']);
			inventory.refresh(value.tag);
			inventory.refresh(value.barcode);
		});
		$.each(list.not_in_location, function(key, value){
			inventory.notInLocationList[value.tag] = value.tag;
			inventory.notInLocationList[value.barcode] = value.barcode;
			value.id = key;
			inventory.display(value, '#nil');
			inventory.display(value, '#inventoryDisplay');
			inventory.removeDuplicates(['#inventoryDisplay']);
			inventory.refresh(value.tag);
			inventory.refresh(value.barcode);
		});
		setTimeout(function(){
			$('#inventoryFunctions button#saveFloorInventoryList').removeAttr('disabled');
			$('#inventoryFunctions button#clearInventory').removeAttr('disabled');
		}, 500);
		$('#inventoryPanel').panel('close');
	},
	saveList: function(){
		// query in_location
		var data = [];
		$('#il').find('pre').each(function(){
			var id = $(this).attr('data-id');
			data.push(id);
		});
		// call 
		if (data.length){
			var d = {data:data};
			api.call('action=proSaveFloorInventoryList&floor='+frb_richmond.floor+'&site='+frb_richmond.site+'&'+$.param(d)+'&token='+user.session.user.token, function(response){
				alert('Inventory Saved');
				//frb_richmond.reset();
			});
		}
	},
	queryListModal: function(){
		// popup modal to query site and inventory
		$("#floorInventoryListModal select#siteSelect").html('<option value="">Please Choose</option>');
		$("#floorInventoryListModal select#floorSelect").html('<option value="">Please Choose</option>');
		$.each(Object.keys(frb_richmond.siteFloorMap), function(key, value){
			$("#floorInventoryListModal select#siteSelect").append('<option value="'+value+'">'+value+'</option>');
		});
		$("#floorInventoryListModal select#floorSelect").selectmenu().selectmenu('refresh', true);
		$("#floorInventoryListModal select#siteSelect").selectmenu().selectmenu('refresh', true);
		$.mobile.changePage("#floorInventoryListModal");
	}
};