/**
 * Port of Simple_DB 
 * Peristent NoSQL database implementation for the cordova engine
 *  Removes the need for localstorage, which has a data limit in cordova and may slow down the application 
 * 
 */
function simpleDBPersistent(type) {
    var type = type+'/';
    var dir = 'filelabelpro/json/';
   // if (!localStorage.getItem('simpleDB')) localStorage.setItem('simpleDB', JSON.stringify({}));
    
    function createDirIfNotExists(){
        var directoryToCheck = dir+type;
        //console.log(directoryToCheck);
        var segmento = directoryToCheck.split('/');
        //console.log(segmento);
        
        var directoryUri = '';
        for (var i = 0; i < segmento.length; i++){
            if (segmento[i]){
                directoryUri += segmento[i]+'/';
                //console.log(directoryUri);
                createDirectory(directoryUri, segmento, i);
            }
        }
    }
    
    function createDirectory(directoryUri, segment, index){
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {
            fs.root.getDirectory(directoryUri, {create: true, exclusive: false},function(){
                ////console.log("created folder: "+segment[index]);
            }, function(){
                //console.log("failed to create folder: "+segment[index]);
            });
        });
    }
    
    createDirIfNotExists();
    
    this.uniqid = function() {
        var d = new Date();
        return 'id' + d.getTime() + Math.floor(Math.random() * 1000000);
    };
    
    this.getFileEntries = function(successCallback, errorCallback){
        var that = this;
        var dirToCheck = dir+type;
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {

            ////console.log('file system open: ' + fs.name);
            fs.root.getDirectory(dirToCheck, { create: false, exclusive: false }, function (directory) {
                
                var directoryReader = directory.createReader();
                directoryReader.readEntries(function(fileEntries){
                    successCallback(fileEntries);
                }, that.fail);
            }, function(){
                errorCallback("Coult not find database entry");
            });
        });
    };
    
    this.check = function(fileName, successCallback, errorCallback){
        var dirToCheck = dir+type;
        var that = this;
        this.getFileEntries(function(fileEntries){
            for (var i=0; i<fileEntries.length; i++) {
                //console.log("getting: "+fileEntries[i].name);
                if (fileEntries[i].name == fileName){
                    errorCallback();
                    break;
                }
            }
            successCallback();
        }, this.fail);
    };
    
    this.readFiles = function(fileEntries, successCallback, errorCallback){
        var filesToRead = fileEntries.length;
        var readCounter = 0;
        var data = {};
        var readError = false;
        if (fileEntries.length > 0){
            //console.log("filestoread: "+filesToRead);
            for (var i=0; i<filesToRead; i++) {
                //console.log("getting: "+fileEntries[i].name);
                fileEntries[i].file(function (file) {
                    var reader = new FileReader();
            
                    reader.onloadend = function() {
                        //console.log("Successful file read: ");
                        //console.log(file.name);
						var ob = {};
                        try {
							ob = JSON.parse(this.result);
						} catch(e){
							console.warn(e);
							console.warn(file.name);
							console.warn(this.result);
						}
                        //console.log(ob);
                        file.name = file.name.replace('.json', '');
                        data[file.name] = ob;
                        readCounter++;
                        //console.log("readcounter: "+readCounter);
                        //console.log("read counter: "+readCounter);
                        //console.log("filesToReadr: "+filesToRead);
                        // check to see if this is the last file and all files have been read
                        if (readCounter >= (filesToRead)){
                            //console.log("calling success callback in readfiles");
                            successCallback(data);
                        }
                        //displayFileData(fileEntry.fullPath + ": " + this.result);
                    };
                    
                    reader.onerror = function(){
                        //console.log("error reading file");
                        readError = true;
                    }
                    
                    reader.readAsText(file);
            
                }, errorCallback);
            }
        } else {
            successCallback({});
        }
    };
    
    this.readFile = function (fileEntry, successCallback, errorCallback) {
    
        fileEntry.file(function (file) {
            var reader = new FileReader();
    
            reader.onloadend = function() {
                //console.log("Successful file read: ");
				var ob = {};
				try {
					ob = JSON.parse(this.result);
				} catch(e){
					console.warn(e);
					console.warn(this.result);
				}
                //console.log(ob);
                successCallback(ob);
                //displayFileData(fileEntry.fullPath + ": " + this.result);
            };
    
            reader.readAsText(file);
    
        }, errorCallback);
    };
    
    this.writeFile = function(fileEntry, dataObj, successCallback, errorCallback) {
        // Create a FileWriter object for our FileEntry (log.txt).
        fileEntry.createWriter(function (fileWriter) {
    
            fileWriter.onwriteend = function() {
                //console.log("Successful file write...");
                //readFile(fileEntry);
                successCallback("success");
            };
    
            fileWriter.onerror = function (e) {
                errorCallback("Failed file write: " + e.toString());
            };
    
            // If data object is not passed in,
            // create a new Blob instead.
            // if (!dataObj) {
            //     dataObj = new Blob(['some file data'], { type: 'text/plain' });
            // }
            
            if (dataObj){
                fileWriter.write(JSON.stringify(dataObj));
            }
        });
    };
    
    this.get = function(key, successCallback, errorCallback) {
        var that = this;
        if (typeof key != 'undefined' && key){
            var fileToCheck = dir+type+key+'.json';
            window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {
    
                //console.log('file system open: ' + fs.name);
                fs.root.getFile(fileToCheck, { create: false, exclusive: false }, function (fileEntry) {
            
                    //console.log("fileEntry is file?" + fileEntry.isFile.toString());
                    // fileEntry.name == 'someFile.txt'
                    // fileEntry.fullPath == '/someFile.txt'
                    that.readFile(fileEntry, successCallback, errorCallback);
            
                }, function(){
                    if (typeof errorCallback !== 'undefined'){
                        errorCallback("Could not find database entry");
                    } else {
                        //console.log("Could not find database entry");
                    }
                });
            
            }, this.fail);
        }
        else {
            // get all objects of type {type}
            var dirToCheck = dir+type;
            this.getFileEntries(function(entries){
                that.readFiles(entries, successCallback, errorCallback);
            }, function(err){
                errorCallback(err);
            });
        }
    };
    
    this.post = function(content, successCallback, errorCallback){
        var key = this.uniqid();
        var that = this;
        this.check(key, function(){
            var fileToCreate = dir+type+key+'.json';
            window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {
    
                //console.log('file system open: ' + fs.name);
                fs.root.getFile(fileToCreate, { create: true, exclusive: false }, function (fileEntry) {
                    that.writeFile(fileEntry, content, successCallback, errorCallback);
                }, function(){
                    errorCallback("failed to get file from root: "+fileToCreate);
                });
            });
        }, 
        function(){
            // re-post with another unique id
            that.post(content, successCallback, errorCallback);
        })
    };
    
    this.put = function(key, content, successCallback, errorCallback){
        var that = this;
        var fileToCreate = dir+type+key+'.json';
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {
            //console.log('file system open: ' + fs.name);
            fs.root.getFile(fileToCreate, { create: true, exclusive: false }, function (fileEntry) {
                that.writeFile(fileEntry, content, successCallback, errorCallback);
            }, function(){
                errorCallback("failed to get file from root: "+fileToCreate);
            });
        });
    };
    
    this.delete = function(key, successCallback, errorCallback) {
        var that = this;
        if (typeof key != 'undefined' && key){
            var fileToCheck = dir+type+key+'.json';
            window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {
                fs.root.getFile(fileToCheck, { create: false, exclusive: false }, function (fileEntry) {
                    //console.log("fileEntry is file?" + fileEntry.isFile.toString());
                    fileEntry.remove(successCallback, errorCallback);
            
                }, function(){
                    errorCallback("Could not find database entry");
                });
            });
        } else {
            // get all objects of type {type}
            var dirToCheck = dir+type;
            window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {
                //console.log('file system open: ' + fs.name);
                fs.root.getDirectory(dirToCheck, {create: true, exclusive: false}, function(de){
                    de.removeRecursively(successCallback, errorCallback);
                }, errorCallback);
            });
        }
    }
    
    // this.check = function(file, method){
    //     var fileToCheck = this.dir+this.type+file+'.json';
    //     fileSystem.root.getFile(fileToCheck, {create: false, exclusive: false}, this.gotFileEntry, function(){
    //         // return unique id
    //         var uniqueid = new Date().getTime();
    //         var fileToCheck = this.dir+this.type+uniqueid+'.json';
    //         console.log(uniqueid);
            
    //         if (this.method == 'post' || this.method == 'put' ){
    //             fileSystem.root.getFile(fileToCheck, {create: true, exclusive: false}, this.gotFileEntry, fail); 
    //         }
    //         else {
    //             console.log("file doesn't exist");
    //         }
    //     });
    // }
    // function fail(){
    //     console.log("simpleDB General Failure");
    // }
    // this.gotFileEntry = function(fileEntry){
        
    //     switch(this.method){
    //         case('get'):
    //             break;
    //         case('post'):
    //             break;
    //         case('put'):
    //             break;
    //         case('delete'):
    //             break;
    //     }
    // }
    // this.getdb = function(){
    //     var db = JSON.parse(localStorage.getItem('simpleDB'));
    //     if (!db) {
    //         localStorage.setItem('simpleDB', JSON.stringify({})); 
    //         db = JSON.parse(localStorage.getItem('simpleDB'));
    //     }
    //     if (!db.hasOwnProperty(this.type)) {
    //         db[this.type] = {}
    //         localStorage.setItem('simpleDB', JSON.stringify(db));
    //         db = JSON.parse(localStorage.getItem('simpleDB'));
    //     }
        
    //     return db;
    // };
    // this.arrayShift = function(inputArr) {
    //     var props = false,
    //         shift = undefined,
    //         pr = '',
    //         allDigits = /^\d$/,
    //         int_ct = -1,
    //         _checkToUpIndices = function(arr, ct, key) {
    //             if (arr[ct] !== undefined) {
    //                 var tmp = ct;
    //                 ct += 1;
    //                 if (ct === key) {
    //                     ct += 1;
    //                 }
    //                 ct = _checkToUpIndices(arr, ct, key);
    //                 arr[ct] = arr[tmp];
    //                 delete arr[tmp];
    //             }
    //             return ct;
    //         };
    //     if (inputArr.length === 0) {
    //         return null;
    //     }
    //     if (inputArr.length > 0) {
    //         return inputArr.shift();
    //     }
    // }
    // this.check = function(key) {
    //     var db = this.getdb();
    //     if (!db[this.type].hasOwnProperty(key)) {
    //         return key;
    //     } else {
    //         return this.check(this.uniqid());
    //     }
    // }
    // this.count = function(mixed_var, mode) {
    //     var key, cnt = 0;
    //     if (mixed_var === null || typeof mixed_var === 'undefined') {
    //         return 0;
    //     } else if (mixed_var.constructor !== Array && mixed_var.constructor !== Object) {
    //         return 1;
    //     }
    //     if (mode === 'COUNT_RECURSIVE') {
    //         mode = 1;
    //     }
    //     if (mode != 1) {
    //         mode = 0;
    //     }
    //     for (key in mixed_var) {
    //         if (mixed_var.hasOwnProperty(key)) {
    //             cnt++;
    //             if (mode == 1 && mixed_var[key] && (mixed_var[key].constructor === Array || mixed_var[key].constructor === Object)) {
    //                 cnt += this.count(mixed_var[key], 1);
    //             }
    //         }
    //     }
    //     return cnt;
    // }
    // this.del = function(key) {
    //     var db = this.getdb();
    //     try {
    //         var type = db[this.type];
    //     } catch (err) {
    //         var type = db[this.type];
    //     }
    //     delete type[key];
    //     db[this.type] = type;
    //     localStorage.setItem('simpleDB', JSON.stringify(db));
    // }

    // this.readFile = function (fileEntry, successCallback) {
    
    //     fileEntry.file(function (file) {
    //         var reader = new FileReader();
    
    //         reader.onloadend = function() {
    //             console.log("Successful file read: ");
    //             var ob = JSON.parse(this.result);
    //             console.log(ob);
    //             successCallback(ob);
    //             //displayFileData(fileEntry.fullPath + ": " + this.result);
    //         };
    
    //         reader.readAsText(file);
    
    //     }, onErrorReadFile);
    // }

    // this.get = function(key, successCallback, errorCallback) {
    //     var that = this;
    //     if (key){
    //         var fileToCheck = this.dir+this.type+key+'.json';
    //         window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {
    
    //             console.log('file system open: ' + fs.name);
    //             fs.root.getFile(fileToCheck, { create: false, exclusive: false }, function (fileEntry) {
            
    //                 console.log("fileEntry is file?" + fileEntry.isFile.toString());
    //                 // fileEntry.name == 'someFile.txt'
    //                 // fileEntry.fullPath == '/someFile.txt'
    //                 that.readFile(fileEntry, successCallback);
            
    //             }, function(){
    //                 errorCallback("Coult not find database entry");
    //             });
            
    //         }, this.fail);
    //     }
    //     else {
            
    //     }
    // }
    // this.getDB = function() {
    //     var db = this.getdb();
    //     return db;
    // }
    // this.getJSON = function(key) {
    //     var db = this.getdb();
    //     return JSON.stringfy(db[this.type][key]);
    // }
    // this.objectToQueryString = function(obj) {
    //     var output = '';
    //     var count = 0;
    //     for (var key in obj) {
    //         if (obj.hasOwnProperty(key)) {
    //             output += key + '=' + obj[key] + '&';
    //         }
    //     }
    //     return output.slice(0, -1);
    // }
    // this.post = function(content) {
    //     var db = this.getdb();
    //     var key = this.check(this.uniqid());
    //     db[this.type][key] = content;
    //     localStorage.setItem('simpleDB', JSON.stringify(db));
    //     return key;
    // }
    // this.parseStr = function(q) {
    //     var arr = {};
    //     var query = String(q).split('&');
    //     for (var key in query) {
    //         if (query.hasOwnProperty(key)) {
    //             var str = String(query[key]).split('=');
    //             var va = str[1];
    //             arr[str[0]] = str[1];
    //         }
    //     }
    //     return arr;
    // }
    // this.put = function(id, content) {
    //     var db = this.getdb();
    //     var key = id;
    //     db[this.type][key] = content;
    //     localStorage.setItem('simpleDB', JSON.stringify(db));
    //     return key;
    // }
    // this.query = function(q) {
    //     var arr = this.parseStr(q);
    //     var get = this.get();
    //     var out = {};
    //     var empty = true;
    //     if (get) {
    //         for (var key in get) {
    //             if (get.hasOwnProperty(key)) {
    //                 var objectString = this.objectToQueryString(get[key]);
    //                 if (this.testQueryArray(arr, objectString)) {
    //                     var empty = false;
    //                     out[key] = get[key];
    //                 }
    //             }
    //         }
    //     }
    //     if (empty) {
    //         return false;
    //     } else {
    //         return out;
    //     }
    // }
    // this.returnSingleId = function(a) {
    //     if (a) {
    //         for (var key in a) {
    //             if (a.hasOwnProperty(key)) {
    //                 return key;
    //             }
    //         }
    //     } else {
    //         return false;
    //     }
    // }, this.shiftArray = function(arr) {
    //     if (this.count(arr) == 1) {
    //         return this.shiftArray(arr);
    //     } else {
    //         return arr;
    //     }
    // }
    // this.testQueryArray = function(array, objectString) {
    //     var testArray = [];
    //     for (var key in array) {
    //         if (array.hasOwnProperty(key)) {
    //             testArray.push(key + '=' + array[key]);
    //         }
    //     }
    //     var count = this.count(testArray);
    //     var testCount = 0;
    //     for (var key in testArray) {
    //         if (testArray.hasOwnProperty(key)) {
    //             if (objectString.indexOf(testArray[key]) !== -1) {
    //                 testCount++;
    //             }
    //         }
    //     }
    //     if (count == testCount) {
    //         return true;
    //     } else {
    //         return false;
    //     }
    // }
    // this.timestamp = function(id) {
    //     var str = String(id).replace('id');
    //     return parseInt(str);
    // }
    // this.toObject = function(arr) {
    //     var rv = {};
    //     for (var key in arr) {
    //         if (arr.hasOwnProperty(key)) {
    //             rv[key] = arr[key];
    //         }
    //     }
    //     return rv;
    // }
    // this.uniqid = function() {
    //     var d = new Date();
    //     return 'id' + d.getTime() + Math.floor(Math.random() * 1000000);
    // }
};