var tsl1128 = {
	scanned: {},
	reads:'',
	isBluetooth:true,
	batteryPercentage:'',
    range:{
        low: -80,
        high: -60
    },
    batteryBar: false,
	closeRangeFinder: function(){
		clearInterval(user.device.finderInterval);
		user.device.reads = '';
		$.mobile.changePage('#view');
	},
	
	data: function(info){
		var html = '';
		$.each(info.data, function(key, value){
			if(key !== 'status' && key !== 'location') {
				if(key == 'image') {
					if(value) {
						html += '<b>Image</b><img src="'+value+'" class="img-thumbnail img-responsive" />';
					}
				} else {
					html += '<b>'+key.replace(/_/g, ' ') +': </b>'+value+'<br />';
				}
			}
		});
		return html;
	},
	
	find: function(t, type){
		var myType = (type == 'undefined' ? 'rfid' : type);
		var apiCallType = (myType == 'rfid' ? 'tag' : 'barcode');
		if(t.length == 6 || t.length == 12 || t.length == 13 || t.length == 24) {
			var found = false;
			api.call('action=proFindTag&'+apiCallType+'='+t+'&token='+user.session.user.token, function(response){
				item.display(response, t, 'rfid');
				$.mobile.loading('hide');
			}, function(){
				$.mobile.loading('hide');
			});
			$('body').trigger('findTag');
		} else {
			//alert('The tag is not valid.');
		}
	},
	
	go: function(data){
		if(typeof data != 'undefined') {
			if(data.indexOf('EP') != -1) {
				$.mobile.loading('show');
				var x = $.trim(data.split(':').pop());
				//console.log(x);
				var a = x.match(/.{1,2}/g);
				var t = '';
				var prefs = new Preferences();
				$.each(a, function(k, v){
					if (prefs.encodingType == enums.encodingType.HEX){
						t += $.trim(String.fromCharCode(hex2dec(v)));
					}
					else {
						t += $.trim(v);
					}
				});
				if(typeof user.device.scanned[t] == 'undefined') {
					user.device.scanned[t] = t;
					if($.mobile.activePage.attr("id") == 'inventory') {
						inventory.init();
					} else {
						user.device.find(t, 'rfid');
					}
				}
			} else if(data.indexOf('BC') != -1) {
				$.mobile.loading('show');
				var x = $.trim(data.split(':').pop());
				if(typeof user.device.scanned[x] == 'undefined') {
					user.device.scanned[x] = x;
					user.device.find(x, 'barcode');
				}
			}
		}
	},

    init: function(t){
		if(cordovaApp) {
			if(typeof user.device == 'undefined') {
				user.changeDevice();
			}
			bluetoothSerial.list(function(devices) {
				devices.forEach(function(device) {
					//console.log("name of the device: "+device.name);
					
					bluetoothSerial.isConnected(
						function(){
							//console.log("device is connected, disconnecting");
							user.device.disconnectBluetoothDevice();
						}, 
						function(){
							//console.log("device is not connected, attempting to connect");
							user.device.connectBluetoothDevice(device);
					});
					
				});
			}, function(){
				$.mobile.loading('hide');
			});
		} else {
			$.mobile.loading('hide');
			if(config.apiUrl.indexOf('filelabel.co') != -1) {
				user.device.find('fafc7ced521e');
			} else {
				user.device.find('57477e6090bc5');
			}
		}
    },
    connectBluetoothDevice: function(device){
		bluetoothSerial.connect(device.id, function(){
			// initialize the battery indicator and connectivity status
			//user.device.initCheckBatteryStatus();
			//console.log("found and connecting to bluetooth device: "+ device.id);
			
			
			$.mobile.loading('hide');
			
		
    		user.device.batteryBar = jQMProgressBar('battery-progress-bar')
		    .setOuterTheme('b')
		    .setInnerTheme('e')
		    .isMini(true)
		    .setMax(100)
		    .setStartFrom(0)
		    .showCounter(true)
		    .build();
    		
			
			bluetoothSerial.subscribe('\r\n', function(data) {
				//The data comes in here one line at a time. That's the problem.
				user.device.reads += data;
				try {
					if($.mobile.activePage.attr("id") != 'rangeFinder') {
						user.device.go(data);
					}
				} catch(e) {}
			});
			
			setTimeout(user.device.initCheckBatteryStatus, 4000);
			user.device.checkConnectionStatus();
		});	
    },
    disconnectBluetoothDevice: function(){
		bluetoothSerial.disconnect();
		clearInterval(user.device.batteryCheckInterval);
		user.device.checkConnectionStatus();
		user.device.batteryPercentage = '';
		$('body').trigger('battery-indicator-update', user.device.batteryPercentage);
    },
    checkBatteryStatus : function(){
    	if(cordovaApp){
    		// look in the reads string for the battery response
    		// once the response is found, set the battery percentage level to the battery widget on the settings screen
    		var percentageVal = false;
    		bluetoothSerial.write('.bl'+"\n", function(){
	    		var info = library.cleanArray(user.device.reads.split("\n"));
	    		for (var i = info.length-1; i >= 0; i--){
	    			var value = info[i];
	    			if (typeof(info[i]) == 'string'){
	    				if (value.indexOf('BP: ') !== -1){
	    					var percentage = $.trim(value.split('BP: ').pop());
	    					percentageVal = percentage.substring(0, percentage.indexOf('%'));
	    					//console.log(percentageVal);
	    					user.device.batteryPercentage = percentageVal;
	    					
	    					// trigger an event to update all battery indicators
	    					$('body').trigger('battery-indicator-update', user.device.batteryPercentage);
	    					break;
	    				}
	    			}
	    		}
    		});
    	}	
    },
    initCheckBatteryStatus: function(){
    	if(cordovaApp){
    		
    		user.device.checkBatteryStatus();
    		
    		user.device.batteryCheckInterval = setInterval(function(){
    			user.device.checkBatteryStatus();
    		}, 60000);
    	}
    },
    rangeFinder: function(t){
    	$('#range').attr('min', -80);
    	$('#range').attr('max', -60);
    	$.mobile.changePage('#rangeFinder');
    	$.mobile.loading('show');
    	var readCount = 0;
    	var resetReadCount = 10;
    	var id = $(t).attr('data-id');
    	api.call('action=proItem&project='+$('#projects').val()+'&id='+id+'&token='+user.session.user.token, function(response){
    		$.mobile.loading('hide');
    		if(cordovaApp && response.output) {
	    		user.device.finderInterval = setInterval(function(){
    				bluetoothSerial.write('.iv -r on'+"\n", function(){
	    				var i = library.cleanArray(user.device.reads.split("\n"));
    					var u = [];
    					$.each(i, function(i, el){
	    					if($.inArray(el, u) === -1) u.push(el);
    					});
    					var info = library.cleanArray(u);
    					var c = 0;
    					var n = {};
    					$.each(info, function(key, value){
	    					var d = c + 1;
	    					if(typeof value == 'string') {
	    						if(value.indexOf('EP: ') != -1) {
	    							if(typeof info[d] != 'undefined') {
	    								if(info[d].indexOf('RI: ') != -1) {
		    								var x = $.trim(value.split(':').pop());
    										var	a = x.match(/.{1,2}/g);
    										var t = '';
	    									$.each(a, function(k, v){
    											t += $.trim(String.fromCharCode(hex2dec(v)));
    										});
    										n[t] = parseInt($.trim(info[d].split(': ').pop()));
    									}
	    							}
    							}
    							c++;
	    					}
    					});
    					$.each(n, function(key, value){
    						if(key == response.output.tag) {
    							$('#range').val(value).slider("refresh");
    						}
    					});
    				});
    			}, 2000);
    		}    		
    	});
    },
    checkConnectionStatus: function(){
    	bluetoothSerial.isConnected(
    	function(){
    		//console.log("bluetooth is connected");
    		$('#connectDevice').text("Disconnect Device");
    		//$('#connectDevice').button('refresh');
    	}, 
    	function(){
    		//console.log("bluetooth is disconnected");
    		$('#connectDevice').text("Connect Device");
    		//$('#connectDevice').button('refresh');
    	});
    }
};