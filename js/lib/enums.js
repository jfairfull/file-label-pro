var enums = {
    encodingType : {
        ASCII: 'ascii',
        HEX: 'hex'
    },
     scanType : {
        RFID: 'RFID',
        BARCODE: 'Barcode'
    },
    rfidMode : {
        REVOKE: 'revoke',
        WRITE: 'write',
        READ: 'read',
        FIND:'find',
        INVENTORY:'inventory'
    } 
};
Object.freeze(enums);