function Preferences(i) {
    this.p = {
        'device':'',
        'apiUrl':'https://flx.filelabel.co/api/'
    }
    var db = new simpleDB('quantumTracking');
    // if(typeof user.session == 'undefined') {
    //     thePrefs = this.p;
    // } else {
        var prefs = db.get("quantumTrackingUser");//db.get(user.session.user.userId);
        if(!prefs) {
            thePrefs = $.extend({}, this.p, i);
        } else {
            thePrefs = $.extend({}, db.get("quantumTrackingUser"), i);
        }
        db.put("quantumTrackingUser", thePrefs);
    //}
    return thePrefs;
}