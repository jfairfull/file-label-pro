var mobileApp = {
    initialize: function() {
        mobileApp.bindEvents();	
        if(cordovaApp) {
	        Click('.extLink', mobileApp.openLink);
        }
    },
	
    bindEvents: function() {
        document.addEventListener('deviceready', mobileApp.onDeviceReady, false);
		document.addEventListener("resume", mobileApp.onResume, false);
		document.addEventListener("pause", mobileApp.onPause, false);
		document.addEventListener("offline", mobileApp.onOffline, false);
		document.addEventListener("online", mobileApp.onOnline, false);
		$('body').trigger('appReady');
    },
	
	clearData: function(){
		window.key = '';
		localStorage.clear();
		user.logOut();
	},

    onDeviceReady: function() {
		window.alert = navigator.notification.alert;
		window.open = cordova.InAppBrowser.open;
		$('body').addClass(String(device.platform).toLowerCase());
    },
    
    onOnline: function(){
    	app.checkSession();	
    },
    
    onOffline: function(){
    	alert('You appear to be offline, please connect to the network to sync with the latest data.');
    },
	
	onResume: function(){
		window.alert = navigator.notification.alert;
		if ((typeof app.checkSessionInterval != 'undefined') && app.checkSessionInterval){
			clearInterval(app.checkSessionInterval);
		}
		
		if (api.checkConnection()){
			app.checkSession();
			app.checkSessionInterval = setInterval(function(){
				app.checkSession();
			}, 300000);	
		}
	},
	
	onPause: function(){
		// if device is connected via bluetooth, disconnect
		if (typeof user.device != 'undefined'){
			if (user.device.isBluetooth){
				
				// get option from preferences on whether or not to disconnect the device on sleep
				// if (typeof user.prefs.disconnectOnPause != 'undefined'){
				// 	if (user.prefs.disconnectOnPause){
				// 			user.device.disconnectBluetoothDevice();		
				// 	}
				// }
			}
		}
		
		if ((typeof app.checkSessionInterval != 'undefined') && app.checkSessionInterval){
			clearInterval(app.checkSessionInterval);
		}
	},
	openLink: function(t){
		var ref = window.open($(t).attr('href'), '_blank', 'location=yes,hardwareback=no');
	}
};