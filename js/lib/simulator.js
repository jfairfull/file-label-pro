var simulator = {
    inventoryInit: function(){
        simulator.scanTags();
        inventory.init();
        inventory.ui();
    },
    
    scanTags: function(){
        user.device.scanned = {
            '416ed76c4daa':'416ed76c4daa',
            'c3f9485f8745':'c3f9485f8745',
            '58643999645c4':'58643999645c4'
        }
    },
	inventoryScanTags: function(){
		if (typeof user.device === 'undefined'){
			user.device = {};
		}
		simulator.scanTags();
		$.each(user.device.scanned, function(i, tag){
			inventory.refresh(tag);
		});
	},
    findTags: function(){
        var tags = ['8d8359755774', 'f9f8eecd9ece', 'a7d5c3ee5722', '9c8b12d32e25', '98dea8a66c24', 'b6d12b25de5b'];
        
        $.each(tags, function(key, val){
           api.call('action=proFindTag&'+'tag'+'='+val+'&token='+user.session.user.token, function(response){
				item.display(response, val, 'rfid');
			}); 
        });
    }
};