var alrh450 = {
    isBluetooth:false,
    scanType: '',
    rfidMode: 'read',
    range:{
        low: -70,
        high: -40
    },
    hasInitialized: false,
    isWriting : false,
    powerLevel: 300,
    myactiveTag: '3000AD9009001B6BFD9662000018',
    tagsInRange: {},
    isReading:false,
	rangeFinderTimeout: 0,
    
    // TODO: create enums/objects for rfid modes and rfid write fail types. might be useful to create a separate atid state manager for modes/boolean flags
    // TODO: implement continuous burst for inventory screen while find view does single read?
    init: function(isReset){
        console.log('ALIEN!!!');
        
        // populate widgets with power level and scan type from preferences, TODO: initialize these elsewhere in a different namespace
        if (typeof isReset == 'undefined'){
            user.device.populatePowerLevel();
            user.device.loadScanType();
        }
        
        if(cordovaApp) {
            user.device.tagsInRange = {};
            user.device.switchScanType('#atid-scanType');
            user.device.enableKeyEvents();
        }
      
    },
    switchScanType : function(t){
        var type = $(t).val();
        switch(type){
            case('RFID'):
                user.device.rfidInit();
                user.device.scanType = type;
                break;
            case('Barcode'):
                user.device.barcodeInit();
                user.device.scanType = type;
                break;
        }
        if (typeof user.prefs == 'undefined'){
            user.prefs = new Preferences();
        }
            
        user.prefs.scanType = user.device.scanType;
        
        user.prefs = new Preferences({'scanType':user.prefs.scanType});
    },
    rfidInit : function() {
        user.device.hasInitialized = false;
        
        //console.log("Initializing alien rfid!");
        
       if (user.device.scanType == 'Barcode'){
		  // console.log("scan type barcode");
            alien.barcode.stop_scan(function(){
			//	console.log("opening reader");
               alien.rfid.open_reader(function() {
				   // console.log("opened reader");
                    user.device.hasInitialized = true;
                    user.device.changePowerLevel();
                }, function(err){
					console.log(err);
				}); 
            }, function(err){
				console.log(err);
			});
       }
       else{
           //atid.rfid.sleep();
		   //console.log("opening reader");
           alien.rfid.open_reader(function(msg) {
               // console.log(msg);
                user.device.reset = false;
				user.device.hasInitialized = true;
            });
       }
       
        // ### called when rfid tag is read ###
        alien.rfid.onReaderReadTag(user.device.readTagCallback, user.device.printMsg);

    },
    readTagCallback : function(data) {
          if (user.device.rfidMode == 'read' || user.device.rfidMode == 'inventory'){
            if($.mobile.activePage.attr("id") != 'rangeFinder') {
				user.device.processingTag = true;
                user.device.go(data.tag);
            }
          }
          else if (user.device.rfidMode == 'write'){
            //console.log("Tag : " + data.tag + " RSSI: " + data.rssi);
            user.device.tagsInRange[data.tag] = data.tag;
            var tag = user.device.processRawTag(data.tag);
            
            if (tag == user.device.activeTag){
                 $.mobile.loading('hide'); 
                 //console.log("RFID Write succeeded");
                 alien.general.onKeyUp(user.device.KeyUpCallback);
                 alien.general.onKeyDown(user.device.KeyDownCallback);
                 user.device.enableUI();
                 
                 alert("RFID Write Succeeded");
            }
            else{
                 user.device.writeRfidError("Verification fail");
            }
          }
          else if (user.device.rfidMode == 'revoke'){
              //console.log("Tag : " + data.tag + " RSSI: " + data.rssi);
              var tag = data.tag.length == 28 ? $.trim(data.tag.substring(4, data.tag.length)) : $.trim(data.tag);
              if (tag == '000000000000000000000000'){
                 $.mobile.loading('hide'); 
                 //console.log("RFID revoke succeeded");
                 user.device.enableKeyEvents();
                 user.device.enableUI();
                 
                 alert("RFID Revoke Succeeded");
            }
            else{
                 user.device.writeRfidError("Possible revoke fail");
            }
          }
          else if (user.device.rfidMode == 'find'){        
			var t = user.device.processRawTag(data.tag);
			if (typeof user.device.tagsInRange[data.tag] == 'undefined'){
				user.device.tagsInRange[data.tag] = [];
			}
			if (user.device.tagToFind == t){
				alien.general.playSound('beep');
				var rssi = data.rssi;
				user.device.tagsInRange[data.tag].push(rssi);
				var numOfReads = user.device.tagsInRange[data.tag].length;
				if (numOfReads >= 10){
					var sum = 0;
					$.each(user.device.tagsInRange[data.tag], function(k, v){
						sum += parseInt(v);
					});
					var averageRSSI = sum/numOfReads;
					//console.log("Tag : " + data.tag + " RSSI: " + averageRSSI);
					delete user.device.tagsInRange[data.tag];
					$('#range').val(averageRSSI).slider("refresh");
				}
			}
          }
    },
    writeTagCallback : function(data){
        //console.log('code: ' + data.code + ' action: ' + data.action + 'epc: ' + data.epc + 'data: ' + data.data);
        
        if (user.device.rfidMode == 'write' || user.device.rfidMode == 'revoke'){
        
            if (data.code == "Out of retries"){
                user.device.writeRfidError(data.code);
                
            }
            else{
                user.device.writeOffset += 1;
                setTimeout(function(){
                    user.device.recursiveWrite(user.device.writeOffset, user.device.writeData);
                    
                }, 1000); 
            }
        }
    },
    barcodeInit : function() {
        //console.log("Initializing alien barcode!");
        user.device.hasInitialized = false;
        if (user.device.scanType == "RFID"){
            alien.rfid.close_reader();
        }
		//console.log("calling on scan listener");
		//console.log(alien.barcode);
        alien.barcode.onScan(
			function(scanResults){
				//console.log(scanResults);
				alien.general.playSound('success');
				//atid.barcode.stopDecode(function(){}, function(){});
				if($.mobile.activePage.attr("id") != 'rangeFinder') {
					user.device.go(scanResults);
				}
			}, function(msg){
				//console.log("barcode on scan listener can't initialize");
				//console.log(msg); 
				//alien.general.playSound('fail');
			}
		);
    },
    //TODO: make the process raw tag and decode processed tag in a separate namespace
    processRawTag: function(data){
        return invengo_device.processRawTag(data);
    },
    decodeProcessedTag: function(tag){
        var data = '';
        for (var i = 0; i < tag.length; i++){
            data += dec2hex(tag.charCodeAt(i));
        }
        
        //console.log(data);
        return data;
    },
    go: function(data){
		if(typeof data != 'undefined') {
			if (user.device.scanType == 'RFID'){
			    if (typeof user.device.raw[data] == 'undefined') {
			        // if we are reading in a new tag into the system, shut down key event listener, stop any scan, process tag/find, then reinitialize
			        user.device.raw[data] = data;
            		$.mobile.loading('show');
            		var t = user.device.processRawTag(data);
            		
            	    //console.log('Encoded RFID value: ' + t);
            	    t = t.toLowerCase();
            		
            		if(typeof user.device.scanned[t] == 'undefined') {
            		    t = t.toLowerCase();
            			
            			switch($.mobile.activePage.attr("id")){
            			    case 'inventory':
            			        alien.general.playSound('beep');
            			        user.device.scanned[t] = t;
            			        inventory.refresh(t);
            			        break;
            			    case 'find':
            			        alien.general.playSound('beep');
            			        user.device.scanned[t] = t;
            			        user.device.find(t, 'rfid');
            			        break;
            			}
            		}
            		else {
            		    user.device.processingTag = false;
					}
    			}
    			else {
    			   user.device.processingTag = false; 
				}
			}
            else if (user.device.scanType == 'Barcode'){
        		$.mobile.loading('show');
        		var x = data;
        		x = x.toLowerCase();
        		if(typeof user.device.scanned[x] == 'undefined') {
        		    x = x.toLowerCase();
        			//user.device.scanned[x] = x;
        			switch($.mobile.activePage.attr("id")){
        			    case 'inventory':
        			        //alien.general.playSound('beep');
        			        user.device.scanned[x] = x;
        			        inventory.refresh(x, 'barcode');
        			        break;
        			    case 'find':
        			        //alien.general.playSound('beep');
        			        user.device.scanned[x] = x;
        			        user.device.find(x, 'barcode');
        			        break;
        			    case 'globalUpdateScanLocation':
        			        // get location value by barcode
        			        var foundLocation = false;
        			        $.each(user.session.locationIDs, function(k, v){
        			            if (v.barcode == x){
        			                foundLocation = k;
        			            }
        			        });
        			        if (foundLocation){
        			            $('#globalUpdateScanLocation input[name="location"]').val(foundLocation);
        			        } else {
        			            alert("Location not found.")
        			        }
        			        $.mobile.loading('hide');
        			        break;
        			}
    			}
            } else {
				user.device.processingTag = false;
			}
		} else {
			user.device.processingTag = false;
		}
	},
	find: function(t, type){
		var myType = (type == 'undefined' ? 'rfid' : type);
		var apiCallType = (myType == 'rfid' ? 'tag' : 'barcode');
		if(apiCallType == 'barcode' || t.length == 8 || t.length == 6 || t.length == 12 || t.length == 13 || t.length == 24) {
			var found = false;
			api.call('action=proFindTag&'+apiCallType+'='+t+'&token='+user.session.user.token, function(response){
				item.display(response, t, 'rfid');
				//console.log("Found tag in remote server!");
				$.mobile.loading('hide');
				user.device.processingTag = false;
			}, function(){
			    //alert("error when reaching findTag endpoint, check network connection");
				$.mobile.loading('hide');
				user.device.processingTag = false;
			});
			$('body').trigger('findTag');
		} else {
			alert('The tag is not valid.');
			//console.log('The tag is not valid.');
			user.device.processingTag = false;
		}
	},
	rangeFinder: function(t){
	    //console.log("Opening range finder");
	    if (user.device.disableKeyEvents()){
        	$('#range').attr('min', user.device.range.low);
        	$('#range').attr('max', user.device.range.high);
        	$.mobile.changePage('#rangeFinder');
        	$.mobile.loading('show');
        	var readCount = 0;
        	var resetReadCount = 10;
        	var id = $(t).attr('data-id');
        	api.call('action=proItem&project='+$('#projects').val()+'&id='+id+'&token='+user.session.user.token, function(response){
        		$.mobile.loading('hide');
        		if(cordovaApp && response.output) {
        		    user.device.tagToFind = response.output.tag;
        		    alien.rfid.isRunning(function(msg){
                          if (msg == 'false'){
                            user.device.isScanning = true;
							//console.log("calling scan function");
                            alien.rfid.start_readTagContinuous(function(){
								//console.log("reading tags");
							});
							
							if (user.device.rangeFinderTimeout > 0){
								user.device.scanInterval = setInterval(function(){
									if (typeof user.device.tagsInRange[user.device.tagToFind] != 'undefined'){
										delete user.device.tagsInRange[user.device.tagToFind];
									} else {
										$('#range').val(user.device.range.low).slider("refresh");
									}
								}, user.device.rangeFinderTimeout);
							} 
                          }
                    });
        		}
        	});
	    }
	},
	scanBurst : function(){
	    if (!user.device.isScanning){
	        return;
	    }
	        
	    alien.rfid.start_readTagSingle(function(data){
			var t = user.device.processRawTag(data.tag);
			if (user.device.tagToFind == t){
				 //console.log("Tag : " + data.tag + " RSSI: " + data.rssi);
				//alien.general.playSound('beep');
				$('#range').val(data.rssi).slider("refresh");
			}
		});
	},
	closeRangeFinder: function(){
	    //console.log("closing range finder");
	    user.device.isScanning = false;
		clearInterval(user.device.scanInterval);
		alien.rfid.stop_scan(function(){
			user.device.tagsInRange = {};
			user.device.tagToFind = '';
			$.mobile.changePage('#view');
			user.device.enableKeyEvents();
		});
	},
	writeRfid : function(data) {
	    user.device.tagsInRange = {};
	    if (data.length != 24){
	        alert("invalid tag to write, aborting..");
	        user.device.enableKeyEvents();
	        return;
	    }
	    
	    user.device.rfidMode = user.device.rfidMode == 'revoke' ? 'revoke' : 'write';
	    $.mobile.loading('show');
	    user.device.disableUI();
	    
	    // The most elegant solution!
	    // disable key events, will return true when complete
	    if (user.device.disableKeyEvents()) {
	       // console.log("disabling key events");
	        
	        // stop any ongoing scans
            alien.rfid.stop_scan(function(){
                // set a timeout in case write never finishes
                user.device.isWriting = true;
               // console.log("writing " + data + " to RFID chip!");
                user.device.writeData = data;
                alien.rfid.start_writeTagMemory(user.prefs.encodingType, user.device.writeData, function(succ){
                  // console.log(succ);
				   alien.rfid.start_readTagSingle(function(data){
					if (user.device.rfidMode == 'write'){
					//	console.log("Tag : " + data.tag + " RSSI: " + data.rssi);
						user.device.tagsInRange[data.tag] = data.tag;
						var tag = user.device.processRawTag(data.tag);
						
						if (tag == user.device.activeTag){
							 $.mobile.loading('hide'); 
							//console.log("RFID Write succeeded");
							 alien.general.onKeyUp(user.device.KeyUpCallback);
							 alien.general.onKeyDown(user.device.KeyDownCallback);
							 user.device.enableUI();
							 
							 alert("RFID Write Succeeded");
						}
						else{
							 user.device.writeRfidError("Verification fail");
						}
					}
					else if (user.device.rfidMode == 'revoke'){
						  //console.log("Tag : " + data.tag + " RSSI: " + data.rssi);
						  var tag = data.tag.length == 28 ? $.trim(data.tag.substring(4, data.tag.length)) : $.trim(data.tag);
						  if (tag == '000000000000000000000000'){
							 $.mobile.loading('hide'); 
							 //console.log("RFID revoke succeeded");
							 user.device.enableKeyEvents();
							 user.device.enableUI();
							 
							 alert("RFID Revoke Succeeded");
						}
						else{
							 user.device.writeRfidError("Possible revoke fail");
						}
					}
				   }, function(msg){user.device.writeRfidError(msg)}, false); 
				   //alert("write successful");
				   //$.mobile.loading('hide');
				   //user.device.enableUI();
                   //user.device.enableKeyEvents();
                }, user.device.writeRfidError);

            },  user.device.writeRfidError);
	    }
	    
	},
	// have to write one word (4 hex chars) at a time due to exception
	writeRfidError : function(msg){
	  clearTimeout(user.device.writeTimeout);
	  user.device.isWriting = false;
	  user.device.rfidMode = 'write';
	  //console.log("Write error");
	  //console.log(msg);
	  alert(msg);
	  alien.rfid.stop_scan(function(){
	      user.device.init();
          alien.general.onKeyUp(user.device.KeyUpCallback);
          alien.general.onKeyDown(user.device.KeyDownCallback);
	         
	      $.mobile.loading('hide');
	      user.device.enableUI();
	  });
	    
	},
	revokeTag: function(){
	    //console.log("Revoking tag!");
        user.device.rfidMode = 'revoke';
        user.device.writeRfid('000000000000000000000000');  
	},
	disableKeyEvents : function(){
	    return (alien.general.onKeyDown() == alien.general.onKeyUp());
	},
	enableKeyEvents : function() {
	    alien.general.onKeyUp(user.device.KeyUpCallback);
        alien.general.onKeyDown(user.device.KeyDownCallback);
	},
    KeyUpCallback : function(key) {
        switch (user.device.scanType){
            case ('RFID'):
                if (key.keyCode == alien.general.scanner_handle_keycode && key.repeatCount <= 0){
                    alien.rfid.stop_scan(function(msg){
                          //  console.log(msg);
                            //console.log("stop scan invoked, called success callback");
                          }, function(msg){
                              console.log(msg);
                              console.log("stop scan invoked, called error callback");
                          });
                }
                break;
            
            case('Barcode'):
                 if (key.keyCode == alien.general.scanner_handle_keycode && key.repeatCount <= 0){
                    alien.barcode.stop_scan();
                 }
                break;
        }
    },
    
    KeyDownCallback : function(key) {
        
        // TODO: additional control logic for more scan types under rfid, such as writing tags.
        // TODO: switch rfid mode on tag write screen, if mode is write, process active tag into hex and send into writerfid function
        switch (user.device.scanType){
            case ('RFID'):
				//console.log("getting scanner keycode");
                if (key.keyCode == alien.general.scanner_handle_keycode && key.repeatCount <= 0){
                    //console.log("getting rfid mode: "+user.device.rfidMode );
					if (user.device.rfidMode == 'read'){
                        if (!user.device.processingTag){
							//console.log("calling is running");
                              alien.rfid.isRunning(function(msg){
								  //console.log("returned from isRunning, response: ");
								  //console.log(msg);
                                  if (msg == 'false'){
                                      //atid.rfid.start_readTagSingle(function(msg){console.log(msg);}, user.device.printMsg /*user.device.rfidReadError*/, false);
                                      //console.log(alien.rfid);
									  //console.log(alien.rfid.start_readTagContinuous);
									  alien.rfid.start_readTagContinuous(function(msg){console.log(msg);}, user.device.printMsg);
                                  }
                              }, user.device.printMsg);
                        }
                    }
                    else if (user.device.rfidMode == 'write' && key.repeatCount <= 0){
                        if (typeof user.device.activeTag == 'undefined' || user.device.activeTag == ''){
                            //console.log("active tag not found, aborting write...");
                            user.device.writeRfidError("active tag not found, aborting write...");
                            return;
                        }
                            
                        var data = '';
                        //console.log(user.prefs.encodingType);
                        //console.log(enums.encodingType.HEX);
                        if (user.prefs.encodingType == enums.encodingType.HEX){
                            for (var i = 0; i < user.device.activeTag.length; i++){
                                data += dec2hex(user.device.activeTag.charCodeAt(i));
                            }
                        } else {
                            data = user.device.activeTag;
                        }
                        
                        //console.log(data);
                        
                        if (user.device.disableKeyEvents())
                        {
                            user.device.writeRfid(data);
                        }
                    }
                    else if (user.device.rfidMode == 'inventory'){
                        alien.rfid.isRunning(function(msg){
                              if (msg == 'false'){
                                  user.device.isReading = true; 
                                  alien.rfid.start_readTagContinuous(function(msg){console.log(msg);});
                              }
                          }, user.device.printMsg);
                    }
                }
                break;
            
            case('Barcode'):
                if (key.keyCode == alien.general.scanner_handle_keycode && key.repeatCount <= 0){
                    alien.barcode.start_scan();
                }
                break;
        }
        
    },
    loadScanType: function(){
        if (typeof user.prefs == 'undefined'){
        	user.prefs = new Preferences();
        }
        
        if (typeof user.prefs.scanType == 'undefined'){
            user.prefs = new Preferences({'scanType':'RFID'});
        }
            
        $('#atid-scanType').val(user.prefs.scanType);
    },
    changePowerLevel: function(t){
        user.device.disableUI();
        
        var $power_selects = $('.power-level');
		var powerLevel = (typeof t == 'undefined') ? '' : $(t).val();
		
		// update preferences with selected power level
		if (typeof user.prefs == 'undefined'){
			user.prefs = new Preferences();
		}
		
		if (powerLevel){
			user.prefs.powerLevel = powerLevel;
			
			// update all other power slider selects
    		$.each($power_selects, function(){
    		    $(this).val(powerLevel).selectmenu().selectmenu('refresh');
    		});
		}
		else if (typeof user.prefs.powerLevel == 'undefined'){
			user.prefs.powerLevel = (typeof user.device.powerLevel) != 'undefined' ? user.device.powerLevel : 30;
		}
	    
	    user.device.powerLevel = user.prefs.powerLevel;
		
		user.prefs = new Preferences({'powerLevel':user.prefs.powerLevel});
		
		// if the device is initialized, set the power level through the atid rfid api
		if (user.device.hasInitialized){
		    //console.log("changing power to: " + user.device.powerLevel);
		    alien.rfid.setPower(user.device.powerLevel, user.device.printMsg, user.device.printMsg); 
		}
		user.device.enableUI();
	},
	populatePowerLevel: function(){
	  // grap select statement by class, dynamically add options 100-300 with increment level 50
	  // once select is populated, make the value = to the loaded preferences option
	  
	  if (typeof user.prefs == 'undefined'){
	    user.prefs = new Preferences();
	  }
	    
	  if (typeof user.prefs.powerLevel == 'undefined'){
	      user.prefs = new Preferences({'powerLevel':'300'});
	  }
	  var $select = $('.power-level');
	  var options = '';
	  
	  for (var i = 0; i <= 30; i+=5)
	  {
	      options += '<option value="'+((i == 0) ? (i+1) : (i))+'">'+((i == 0) ? (i+1) : (i))+'</option>';
	  }
	  
	  $.each($select, function(){
	      $(this).html(options);
	      $(this).val( user.prefs.powerLevel ).selectmenu().selectmenu('refresh');
	  });
	  
	},
	switchRfidMode : function(mode){
	  switch(mode){
	      case('read'):
	      case('write'):
	          user.device.rfidMode = mode;
	          break;
	      default:
	        //console.log("Invalid rfid mode set");
	  }  
	},
    printMsg : function(msg) {console.log(msg)},
    disableUI : function(){
        $("body").prepend("<div class=\"overlay\"></div>");

        $(".overlay").css({
            "position": "absolute", 
            "width": $(document).width(), 
            "height": $(document).height(),
            "z-index": 99999,
            "opacity":0.8,
        }).fadeTo(0, 0.8);
    },
    enableUI : function(){
      $(".overlay").remove();  
    },
    raw:{},
    scanned:{}
};