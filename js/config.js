var db = new simpleDB('config');
config = db.get('config');
if(!config) {
    db.put('config', {
        apiUrl:(samApp ? samApp : 'https://flx.filelabel.co/api/')
    });
    config = db.get('config');
}